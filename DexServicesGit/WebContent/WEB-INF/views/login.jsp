<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en" class="no-js">
    <head>
<title>CAREER CRAFTING ZONE | Login :: CCZ</title>
<!-- for-mobile-apps -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Responsive web, Bootstrap Web, Flat Web, Android Compatible web, Smartphone Compatible web, webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //for-mobile-apps -->
<link href="resources/css/cbp_bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="resources/css/cbp_style.css" rel="stylesheet" type="text/css" media="all" />
<!-- js -->
<script src="resources/js/jquery-1.11.1.min.js"></script>
<!-- //js -->
<script src="resources/js/modernizr.custom.js"></script>
<!-- fonts -->
<link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Alegreya:400,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
<!-- //fonts -->
	<!-- start-smoth-scrolling -->
		<script type="text/javascript" src="resources/js/move-top.js"></script>
		<script type="text/javascript" src="resources/js/easing.js"></script>
		<script type="text/javascript">
			jQuery(document).ready(function($) {
				$(".scroll").click(function(event){		
					event.preventDefault();
					$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
				});
			});
		</script>
	<!-- start-smoth-scrolling -->

     
    
</head>
    <body>
   
      <a href="index.jsp">  <button  class="buttton buton2">Back</button></a>
        <!-- header -->
<!-- <div class="header">
		<div class="container">
			<div class="header-nav">
				<nav class="navbar navbar-default">
					Brand and toggle get grouped for better mobile display
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
					    <h1><a class="navbar-brand" href="index.jsp"><i class="glyphicon glyphicon-education" aria-hidden="true"></i><span>Career</span> Crafting Zone</a></h1>
					</div>
					Collect the nav links, forms, and other content for toggling
					<div class="collapse navbar-collapse nav-wil" id="bs-example-navbar-collapse-1">
						<ul class="nav navbar-nav">
							<li><a class="hvr-overline-from-center button2" href="index.jsp">Home</a></li>
							<li><a class="hvr-overline-from-center button2" href="about.jsp">About</a></li>
							<li><a class="hvr-overline-from-center button2" href="services.jsp">Services</a></li>
							<li><a class="hvr-overline-from-center button2" href="contact.jsp">Contact</a></li>
                            <li><a class="hvr-overline-from-center button2" href="faqs.jsp">FAQs</a></li>
                            <li><a class="hvr-overline-from-center button2" href="chat.jsp">Chat</a></li>
                            <li><a class="hvr-overline-from-center button2 active" href="login">Login</a></li>
						</ul>
						 <div class="search-box">
							<div id="sb-search" class="sb-search">
								<form>
									<input class="sb-search-input" placeholder="Enter your search term..." type="search" name="search" id="search">
									<input class="sb-search-submit" type="search" value="">
									<span class="sb-icon-search"> </span>
								</form>
							</div>
						</div>
					</div>
					/navbar-collapse
					
					search-scripts
					<script src="resources/js/classie.js"></script>
					<script src="resources/js/uisearch.js"></script>
						<script>
							new UISearch( document.getElementById( 'sb-search' ) );
						</script>
					//search-scripts
				</nav>
			</div>
		</div>
</div> -->
<!-- header -->
<!-- banner -->
<!--<div class="banner page-head">-->
    <!--  <div class="image">

     <a href=""><img src="resources/images/cbp_images/contact.jpg" width="1349" height="350" alt=""></a>
         <a href=""><img src="resources/images/cbp_images/faq.jpg"  alt="About Us"></a>

 </div> -->
<!--</div>-->
<!-- //banner -->
        <style>
form {
    width: 50%;
    height: auto;
    margin: auto;
    
}
.label{
font-size: 22px;
color: red;

}

.buton {
     background-color: #4CAF50; /* Green #FE9A2E*/
    border: none;
    color: white;
    padding: 10px 72px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 22px;
    margin: 4px 2px;
    -webkit-transition-duration: 0.4s; /* Safari */
    transition-duration: 0.3s;
    cursor: pointer;
}
.buttton {
     background-color: #4CAF50; /* Green #FE9A2E*/
    border: none;
    color: white;
    padding: 3px 15px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 15px;
    margin: 4px 20px;
    -webkit-transition-duration: 0.4s; /* Safari */
    transition-duration: 0.3s;
    cursor: pointer;
}

.buton1 {
    background-color: #FE9A2E;
    color: white;
    border: 2px solid #FE9A2E;
}

.buton1:hover {
    background-color: #4CAF50;
    color: white;
}
.buton2 {
    background-color: #2E86C1;
    color: white;
    border: 2px solid 5470FC;
}

.buton2:hover {
    background-color: green;
    color: white;
}
</style>
        <div class="container">
           
             <%-- <header>
                
				<nav class="codrops-demos">
				<% String msg=(String)request.getAttribute("msg");
					if(msg== null){
						msg=(String)request.getParameter("msg");
					}
				
				 if(msg!=null){%>
				<span style="color: maroon; font-size: large;"> <strong><%=msg %></strong></span>
				
				<%	}else{%>
				<span> <strong></strong>  <strong></strong></span>
				<%} %> 
				
					
					
				</nav>
            </header> --%>
            <section>				
                <div id="container_demo" >
                    <!-- hidden anchor  -->
                   <!--  <a class="hiddenanchor" id="toregister"></a>
                    <a class="hiddenanchor" id="tologin"></a>
                    <div id="wrapper"> -->
                    <center>
		<img style="height: 50%; width:40%; align:center" src="resources/images/cbp_images/login.png" alt="Register Here"/>
		
		
		<header>
                
				<nav class="codrops-demos">
				<% String msg=(String)request.getAttribute("msg");
					if(msg== null){
						msg=(String)request.getParameter("msg");
					}
				
				 if(msg!=null){%>
				<span style="color: red; font-size: large;"> <strong><%=msg %></strong></span>
				
				<%	}else{%>
				<span> <strong></strong>  <strong></strong></span>
				<%} %> 
				
					
					
				</nav>
            </header>
            
            
		</center>
		
		
		             <div id="login" class="animate form">
                            <form name="f1"  action="validateLogin" method="get" autocomplete="on"> 
                               <label style="color:#393939;font-size:20px">Your User Id </label>
				<div class="input-group input-group-lg">
  <span class="input-group-addon" id="basic-addon1" >	&#128104;</span><!-- style="color:#393939;font-size:20px" -->
  <input type="text" name="userName" class="form-control" placeholder="Enter Your User Id" aria-describedby="basic-addon1">
  
    
</div>                               <!-- <h1>Login</h1> 
                                <p> 
                                    <label for="username" class="uname" data-icon="u" > Your User Name </label>
                                    <input id="username" name="userName" required="required" type="text" placeholder="UserName"/>
                                </p> -->
                                <label style="color:#393939;font-size:18px">Password </label>
<div class="input-group input-group-lg">
 <span class="input-group-addon" id="basic-addon1">	&#128273;</span>
  <input type="password" name="password" class="form-control" placeholder="******" aria-describedby="basic-addon1">
    
</div>
<!-- <label style="color:#FE9A2E;font-size:15px" > <br> &#128274;    Forgot Password? <a href="forgotpassword.jsp"><button class="buttton buton2"><a href="forgotpassword.jsp">Click here</a></button> </a> <br>	&#9998;    Not Registered ? &nbsp;&nbsp;<a href="stdRegistration"><button class="buttton buton2" > <a href="stdRegistration">JOIN US !</a></button></a></label>
 -->
 <p style="color:#FE9A2E;font-size:15px" ><a href="forgotpassword.jsp">&#128274;    <u>Forgot Password?</u></a></p>
 <p style="color:#FE9A2E;font-size:15px" ><a href="studentRegistrationPage">&#9998;    <u>Craete a Account</u></a></p>
 
 
 
 
<!--  <label style="color:#FE9A2E;font-size:15px" > <br> &#128274;    Forgot Password? 
 <button  class="buttton buton2"><a href="forgotpassword.jsp">Recover Password</a></button>
  <br>	&#9998;    Not Registered ? &nbsp;
  <input class="buttton buton2" type="button" value="Register Now !" onclick="newDoc()" >
  <button class="buttton buton2" href="#"><a href="studentRegistrationPage">Register Now !</a></button>
   </label> -->
    <center>
 
  <button type="submit" class="buton buton1">LOGIN</button>
</center> </form>    
  </div>
  </div>
</section>
        </div>

<!-- for bootstrap working -->
	<script src="resources/js/bootstrap.js"></script>
<!-- //for bootstrap working -->
<!-- smooth scrolling -->
	<script type="text/javascript">
		$(document).ready(function() {
		/*
			var defaults = {
			containerID: 'toTop', // fading element id
			containerHoverID: 'toTopHover', // fading element hover id
			scrollSpeed: 1200,
			easingType: 'linear' 
			};
		*/								
		$().UItoTop({ easingType: 'easeOutQuart' });
		});
	</script>
	<a href="#" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
<!-- //smooth scrolling -->
<script type="text/javascript">
function newDoc() {
    window.location.assign("/vcu/studentRegistration")
}

</script>
    </body>
</html>