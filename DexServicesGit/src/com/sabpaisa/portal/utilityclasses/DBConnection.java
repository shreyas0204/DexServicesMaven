package sabpaisa.portal.utilityclasses;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Properties;
import java.util.StringTokenizer;

import org.apache.log4j.Logger;


public class DBConnection {
	Logger log = Logger.getLogger(DBConnection.class);
	ExternalProperties chClass = new ExternalProperties();
	Properties cProps = chClass.getPropValues();

	public Connection getConnection() {
		Connection con = null;
		String ip = cProps.getProperty("lp_host");
		String port = cProps.getProperty("lp_port");
		String dbName = cProps.getProperty("lp_dbName");
		String username = cProps.getProperty("lp_userName");
		String password = cProps.getProperty("lp_password");
		String DB_URL = "jdbc:mysql://" + ip + ":" + port + "/" + dbName + "";
		log.info("DB_URL == " + DB_URL);
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {

			e.printStackTrace();
		}

		try {
			con = DriverManager.getConnection(DB_URL, username, password);
			log.info( dbName+" Database Connected :)");
			
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return con;
	}
	
	
	public static void main(String[] args) {
		DBConnection conn=new DBConnection();
	      
	      System.out.println("SubString String is ="+conn.getConnection());  
	}
}
