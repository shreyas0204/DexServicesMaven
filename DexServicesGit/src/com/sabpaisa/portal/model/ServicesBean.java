package sabpaisa.portal.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@Entity
@Table(name = "organization_services")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonDeserialize
public class ServicesBean {
	@GenericGenerator(name = "g1", strategy = "increment")
	@Id
	@GeneratedValue(generator = "g1")
	private Integer serviceId;
	private String services_description;
	private String services_name;
	public String getServices_name() {
		return services_name;
	}
	public void setServices_name(String services_name) {
		this.services_name = services_name;
	}
	@Column(insertable = false, updatable = false)
	private Integer product_id_fk;
	@ManyToOne(targetEntity = OrganizationProductBean.class)
	@JoinColumn(name = "product_id_fk", referencedColumnName = "productId")
	private OrganizationProductBean productBean;
	
	public Integer getServiceId() {
		return serviceId;
	}
	public Integer getProduct_id_fk() {
		return product_id_fk;
	}
	public void setProduct_id_fk(Integer product_id_fk) {
		this.product_id_fk = product_id_fk;
	}
	public OrganizationProductBean getProductBean() {
		return productBean;
	}
	public void setProductBean(OrganizationProductBean productBean) {
		this.productBean = productBean;
	}
	public void setServiceId(Integer serviceId) {
		this.serviceId = serviceId;
	}
	public String getServices_description() {
		return services_description;
	}
	public void setServices_description(String services_description) {
		this.services_description = services_description;
	}
	
	  
}
