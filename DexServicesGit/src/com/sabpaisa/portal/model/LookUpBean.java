package sabpaisa.portal.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;


@Entity
@Table(name = "lookup_productType")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonDeserialize
public class LookUpBean {

	@GenericGenerator(name = "g1", strategy = "increment")
	@Id
	@GeneratedValue(generator = "g1")
	private Integer lookupId;
	@Column(unique = true)
	private String lookup_productType;
	public Integer getLookupId() {
		return lookupId;
	}
	public void setLookupId(Integer lookupId) {
		this.lookupId = lookupId;
	}
	public String getLookup_productType() {
		return lookup_productType;
	}
	public void setLookup_productType(String lookup_productType) {
		this.lookup_productType = lookup_productType;
	}
}
