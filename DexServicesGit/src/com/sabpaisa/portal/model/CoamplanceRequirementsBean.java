package sabpaisa.portal.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@Entity
@Table(name = "compliance_category_requirements")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonDeserialize
public class CoamplanceRequirementsBean {
	@GenericGenerator(name = "g1", strategy = "increment")
	@Id
	@GeneratedValue(generator = "g1")
	private Integer ID;
	private Integer category_id_fk,compliance_id_fk,document_id_fk;
	public Integer getID() {
		return ID;
	}
	public void setID(Integer iD) {
		ID = iD;
	}
	public Integer getCategory_id_fk() {
		return category_id_fk;
	}
	public void setCategory_id_fk(Integer category_id_fk) {
		this.category_id_fk = category_id_fk;
	}
	public Integer getCompliance_id_fk() {
		return compliance_id_fk;
	}
	public void setCompliance_id_fk(Integer compliance_id_fk) {
		this.compliance_id_fk = compliance_id_fk;
	}
	public Integer getDocument_id_fk() {
		return document_id_fk;
	}
	public void setDocument_id_fk(Integer document_id_fk) {
		this.document_id_fk = document_id_fk;
	}

}
