package sabpaisa.portal.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.sql.Update;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@Entity
@Table(name = "login_master")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonDeserialize
public class LoginBean implements Serializable {
	@GenericGenerator(name = "g1", strategy = "increment")
	@Id
	@GeneratedValue(generator = "g1")
	private Integer loginId;
	@Column(unique = true)
     private String userName;
	private String password;
	private String profile;
	@Column(columnDefinition = "varchar(10) default 'pending'")
	private String status;
	
	private Integer mobile_no;
	private String refrenceId;
	
	@OneToOne(cascade = CascadeType.ALL, targetEntity = SuperAdminBean.class)
	@JoinColumn(name = "super_Admin_Id_Fk", referencedColumnName = "saId")
	private  SuperAdminBean saBean;
	
	@OneToOne(cascade = CascadeType.ALL, targetEntity = OrganizationsBean.class)
	@JoinColumn(name = "org_Id_Fk", referencedColumnName = "organizationId")
	private  OrganizationsBean orgBean;
	
	@OneToOne(cascade = CascadeType.ALL, targetEntity = UsersBean.class)
	@JoinColumn(name = "userId_Fk", referencedColumnName = "userId")
	private  UsersBean userBean;
	
	public UsersBean getUserBean() {
		return userBean;
	}
	public void setUserBean(UsersBean userBean) {
		this.userBean = userBean;
	}
	public OrganizationsBean getOrgBean() {
		return orgBean;
	}
	public void setOrgBean(OrganizationsBean orgBean) {
		this.orgBean = orgBean;
	}
	public SuperAdminBean getSaBean() {
		return saBean;
	}
	public void setSaBean(SuperAdminBean saBean) {
		this.saBean = saBean;
	}
	public Integer getLoginId() {
		return loginId;
	}
	public void setLoginId(Integer loginId) {
		this.loginId = loginId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getProfile() {
		return profile;
	}
	public void setProfile(String profile) {
		this.profile = profile;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Integer getMobile_no() {
		return mobile_no;
	}
	public void setMobile_no(Integer mobile_no) {
		this.mobile_no = mobile_no;
	}
	public String getRefrenceId() {
		return refrenceId;
	}
	public void setRefrenceId(String refrenceId) {
		this.refrenceId = refrenceId;
	}
	
	
	
	
}
