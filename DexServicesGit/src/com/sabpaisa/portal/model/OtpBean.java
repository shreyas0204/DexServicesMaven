/**
 * 
 */
package sabpaisa.portal.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

/**
 * @author Aditya
 * Mar 10, 2017
 */

@Entity
@Table(name = "otp_master")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonDeserialize
public class OtpBean {
	@GenericGenerator(name = "g1", strategy = "increment")
	@Id
	@GeneratedValue(generator = "g1")
	private Integer otp_id;
	
	private String otp;
	private String mobile_no;
	private Date otp_generated_date;
	private String isExpired;//N or Y
	public Integer getOtp_id() {
		return otp_id;
	}
	public void setOtp_id(Integer otp_id) {
		this.otp_id = otp_id;
	}
	public String getOtp() {
		return otp;
	}
	public void setOtp(String otp) {
		this.otp = otp;
	}
	public String getMobile_no() {
		return mobile_no;
	}
	public void setMobile_no(String mobile_no) {
		this.mobile_no = mobile_no;
	}
	public Date getOtp_generated_date() {
		return otp_generated_date;
	}
	public void setOtp_generated_date(Date otp_generated_date) {
		this.otp_generated_date = otp_generated_date;
	}
	public String getIsExpired() {
		return isExpired;
	}
	public void setIsExpired(String isExpired) {
		this.isExpired = isExpired;
	}
	
	
}
