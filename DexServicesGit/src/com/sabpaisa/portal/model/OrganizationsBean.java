package sabpaisa.portal.model;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@Entity
@Table(name = "organization_master")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonDeserialize
public class OrganizationsBean {
	
	@GenericGenerator(name = "g1", strategy = "increment")
	@Id
	@GeneratedValue(generator = "g1")
	private Integer organizationId;
	@Column(unique = true)
	private String organization_name;
	private String relationship_type;
	
	private String firstName, lstName, address, gender, mobileNum, emailId;
	@JsonIgnore
	@OneToOne(cascade = CascadeType.ALL)
	LoginBean loginBean;
	public LoginBean getLoginBean() {
		return loginBean;
	}
	public void setLoginBean(LoginBean loginBean) {
		this.loginBean = loginBean;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLstName() {
		return lstName;
	}
	public void setLstName(String lstName) {
		this.lstName = lstName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getMobileNum() {
		return mobileNum;
	}
	public void setMobileNum(String mobileNum) {
		this.mobileNum = mobileNum;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	/*@JsonIgnore
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "orgBean", fetch = FetchType.EAGER)
	private Set<OrganizationProductBean> product;*/

	/*@JsonIgnore
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "orgBean", fetch = FetchType.EAGER)
	private Set<OrganizationsProjectsBean> project;*/

	/*@JsonIgnore
	@OneToMany(cascade=CascadeType.ALL,mappedBy="orgBean",fetch=FetchType.EAGER)
	private Set<PostsBean> post;
	public Set<PostsBean> getPost() {
		return post;
	}
	public void setPost(Set<PostsBean> post) {
		this.post = post;
	}*/
	/*@JsonIgnore
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "orgBean", fetch = FetchType.EAGER)
	private Set<UsersBean> user;*/
	
	
	
	/*@JsonIgnore
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "orgBean", fetch = FetchType.EAGER)
	private Set<RolesBean> role;*/
	
	/*@JsonIgnore
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "orgBean", fetch = FetchType.EAGER)
	private Set<GroupBean> group;*/
	
	/*@JsonIgnore
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "orgBean", fetch = FetchType.EAGER)
	private Set<GroupMembersBean> groupMember;*/
	
	/*@JsonIgnore
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "orgBean", fetch = FetchType.EAGER)
	private Set<FeedsBean> feeds;*/
	
	/*@JsonIgnore
	@ManyToMany(cascade = CascadeType.ALL,fetch=FetchType.EAGER)
	@JoinTable(name = "organizations_users", joinColumns = @JoinColumn(name = "organizationId"), inverseJoinColumns = @JoinColumn(name = "userId"))
    private Set<UsersBean> orguserBean;*/
	
	/*@OneToOne(cascade = CascadeType.ALL)
	private LoginBean loginBean;*/
	
	/*@ManyToOne(cascade=CascadeType.ALL,targetEntity=PartnerDetailsBean.class)
	@JoinColumn(name="reffer_Partner_Idfk",referencedColumnName="partnerId")
	private PartnerDetailsBean partnerDetailsBean;*/
	
	/*public Set<OrganizationProductBean> getProduct() {
		return product;
	}
	public void setProduct(Set<OrganizationProductBean> product) {
		this.product = product;
	}
	public Set<FeedsBean> getFeeds() {
		return feeds;
	}
	public void setFeeds(Set<FeedsBean> feeds) {
		this.feeds = feeds;
	}
	
	
	public Set<UsersBean> getOrguserBean() {
		return orguserBean;
	}
	public void setOrguserBean(Set<UsersBean> orguserBean) {
		this.orguserBean = orguserBean;
	}
	
	
	
	public Set<GroupMembersBean> getGroupMember() {
		return groupMember;
	}
	public void setGroupMember(Set<GroupMembersBean> groupMember) {
		this.groupMember = groupMember;
	}
	public Set<GroupBean> getGroup() {
		return group;
	}
	public void setGroup(Set<GroupBean> group) {
		this.group = group;
	}
	public Set<RolesBean> getRole() {
		return role;
	}
	public void setRole(Set<RolesBean> role) {
		this.role = role;
	}
	//RolesBean
	public Set<UsersBean> getUser() {
		return user;
	}
	public void setUser(Set<UsersBean> user) {
		this.user = user;
	}*/
	public Integer getOrganizationId() {
		return organizationId;
	}
	public void setOrganizationId(Integer organizationId) {
		this.organizationId = organizationId;
	}
	public String getOrganization_name() {
		return organization_name;
	}
	public void setOrganization_name(String organization_name) {
		this.organization_name = organization_name;
	}
	public String getRelationship_type() {
		return relationship_type;
	}
	public void setRelationship_type(String relationship_type) {
		this.relationship_type = relationship_type;
	}
	
	
}
