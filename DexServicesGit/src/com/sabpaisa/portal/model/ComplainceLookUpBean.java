package sabpaisa.portal.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@Entity
@Table(name = "compliance_lookup")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonDeserialize
public class ComplainceLookUpBean {
	@GenericGenerator(name = "g1", strategy = "increment")
	@Id
	@GeneratedValue(generator = "g1")
	private Integer compliance_ID;
	private String compliance_descriptions;
	public Integer getCompliance_ID() {
		return compliance_ID;
	}
	public void setCompliance_ID(Integer compliance_ID) {
		this.compliance_ID = compliance_ID;
	}
	public String getCompliance_descriptions() {
		return compliance_descriptions;
	}
	public void setCompliance_descriptions(String compliance_descriptions) {
		this.compliance_descriptions = compliance_descriptions;
	}

}
