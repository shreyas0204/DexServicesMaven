package sabpaisa.portal.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@Entity
@Table(name = "organization_groups_member")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonDeserialize
public class GroupMembersBean {
	@GenericGenerator(name = "g1", strategy = "increment")
	@Id
	@GeneratedValue(generator = "g1")
	private Integer memberId;
	private String membership_status;
	
	

	@ManyToOne(targetEntity = OrganizationsBean.class)
	@JoinColumn(name = "organization_id_fk", referencedColumnName = "organizationId")
	private OrganizationsBean orgBean;

	@ManyToOne(targetEntity = GroupBean.class)
	@JoinColumn(name = "group_id_fk", referencedColumnName = "groupId")
	private GroupBean groupBean;
	
	public GroupBean getGroupBean() {
		return groupBean;
	}

	public void setGroupBean(GroupBean groupBean) {
		this.groupBean = groupBean;
	}

	public UsersBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UsersBean userBean) {
		this.userBean = userBean;
	}

	@ManyToOne(targetEntity = UsersBean.class)
	@JoinColumn(name = "user_id_fk", referencedColumnName = "userId")
	private UsersBean userBean;

	public Integer getMemberId() {
		return memberId;
	}

	public void setMemberId(Integer memberId) {
		this.memberId = memberId;
	}

	public String getMembership_status() {
		return membership_status;
	}

	public void setMembership_status(String membership_status) {
		this.membership_status = membership_status;
	}

	public OrganizationsBean getOrgBean() {
		return orgBean;
	}

	public void setOrgBean(OrganizationsBean orgBean) {
		this.orgBean = orgBean;
	}
	
	
}
