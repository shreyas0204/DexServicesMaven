package sabpaisa.portal.model;

import java.sql.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "partners_master")
@JsonIgnoreProperties(ignoreUnknown = true)
public class PartnerDetailsBean {
	@GenericGenerator(name = "g1", strategy = "increment")
	@Id
	@GeneratedValue(generator = "g1")
	private Integer partnerId;
	private String partnerName;
	private String gender;
	private String email;
	private String occupation;
	@JsonIgnore
	@OneToOne(cascade=CascadeType.ALL)
	private LoginBean loginBean;

   @ManyToOne(targetEntity=OrganizationsProjectsBean.class)
	
	/*@JsonIgnore
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "partnerDetailsBean", fetch = FetchType.EAGER)
	private Set<OrganizationsBean> orgBean;*/
	
	/*public Set<OrganizationsBean> getOrgBean() {
		return orgBean;
	}
	public void setOrgBean(Set<OrganizationsBean> orgBean) {
		this.orgBean = orgBean;
	}*/
	public LoginBean getLoginBean() {
		return loginBean;
	}
	public void setLoginBean(LoginBean loginBean) {
		this.loginBean = loginBean;
	}
	public Integer getPartnerId() {
		return partnerId;
	}
	public void setPartnerId(Integer partnerId) {
		this.partnerId = partnerId;
	}
	public String getPartnerName() {
		return partnerName;
	}
	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getOccupation() {
		return occupation;
	}
	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}
	public String getPartNerType() {
		return partNerType;
	}
	public void setPartNerType(String partNerType) {
		this.partNerType = partNerType;
	}
	public Integer getParentPartNerId() {
		return parentPartNerId;
	}
	public void setParentPartNerId(Integer parentPartNerId) {
		this.parentPartNerId = parentPartNerId;
	}
	public Date getRegisteredDate() {
		return registeredDate;
	}
	public void setRegisteredDate(Date registeredDate) {
		this.registeredDate = registeredDate;
	}
	public String getContactNumber() {
		return contactNumber;
	}
	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	private String partNerType;
	private Integer parentPartNerId;
	private Date registeredDate;
	private String contactNumber;
	private String address;
}
