package sabpaisa.portal.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@Entity
@Table(name = "project_Details")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonDeserialize
public class ProjectDetailsBean {
	@GenericGenerator(name = "g1", strategy = "increment")
	@Id
	@GeneratedValue(generator = "g1")
	private Integer ID;
	
	private String startDate,endDate,projectOwner,projectPriority,projectSize,projectType;
	
	private String resourcesCount;
	
	private String scope;
	private String currentStatus;
	
	
	
	public String getResourcesCount() {
		return resourcesCount;
	}
	public void setResourcesCount(String resourcesCount) {
		this.resourcesCount = resourcesCount;
	}
	public String getScope() {
		return scope;
	}
	public void setScope(String scope) {
		this.scope = scope;
	}
	public String getCurrentStatus() {
		return currentStatus;
	}
	public void setCurrentStatus(String currentStatus) {
		this.currentStatus = currentStatus;
	}
	@OneToOne(cascade=CascadeType.ALL,targetEntity=OrganizationsProjectsBean.class)
	@JoinColumn(name="project_Idfk",referencedColumnName="project_ID")
	private OrganizationsProjectsBean orgProjectBean;
	
	public OrganizationsProjectsBean getOrgProjectBean() {
		return orgProjectBean;
	}
	public void setOrgProjectBean(OrganizationsProjectsBean orgProjectBean) {
		this.orgProjectBean = orgProjectBean;
	}
	public Integer getID() {
		return ID;
	}
	public void setID(Integer iD) {
		ID = iD;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getProjectOwner() {
		return projectOwner;
	}
	public void setProjectOwner(String projectOwner) {
		this.projectOwner = projectOwner;
	}
	public String getProjectPriority() {
		return projectPriority;
	}
	public void setProjectPriority(String projectPriority) {
		this.projectPriority = projectPriority;
	}
	public String getProjectSize() {
		return projectSize;
	}
	public void setProjectSize(String projectSize) {
		this.projectSize = projectSize;
	}
	public String getProjectType() {
		return projectType;
	}
	public void setProjectType(String projectType) {
		this.projectType = projectType;
	}
	
	
	
}
