package sabpaisa.portal.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@Entity
@Table(name = "segments_lookup")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonDeserialize
public class SegmentsLookUpBean {
	@GenericGenerator(name = "g1", strategy = "increment")
	@Id
	@GeneratedValue(generator = "g1")
	private  Integer segments_ID;
	public Integer getSegments_ID() {
		return segments_ID;
	}
	public void setSegments_ID(Integer segments_ID) {
		this.segments_ID = segments_ID;
	}
	public String getSegments_descriptions() {
		return segments_descriptions;
	}
	public void setSegments_descriptions(String segments_descriptions) {
		this.segments_descriptions = segments_descriptions;
	}
	private String segments_descriptions;

}
