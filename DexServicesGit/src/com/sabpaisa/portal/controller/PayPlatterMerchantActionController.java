package sabpaisa.portal.controller;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import payplatter.bean.MerchantBean;
import payplatter.bean.MerchantServicesListBean;
import payplatter.dao.MerchantDAO;

@RestController
public class PayPlatterMerchantActionController {
	static Logger log = Logger.getLogger(PayPlatterMerchantActionController.class);

	@Autowired
	sabpaisa.portal.dao.LinkPortDAO linkPortDAO;

	@RequestMapping(value = "/getPayerMerchantList/{Pid}", method = RequestMethod.GET, headers = "Accept=application/json")
	public List<MerchantBean> getPayerMerchantList(@PathVariable String Pid) {
		MerchantDAO mDAO = new MerchantDAO();
		List<MerchantBean> merchantList = new ArrayList<>();

		try {
			merchantList = mDAO.getMerchantList(Pid);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return merchantList;
	}

	@RequestMapping(value = "/getMerchantServicesList/{Mid}", method = RequestMethod.GET, headers = "Accept=application/json")
	public List<MerchantServicesListBean> getMerchantServicesList(@PathVariable String Mid) {
		MerchantDAO mDAO = new MerchantDAO();
		List<MerchantServicesListBean> merchantServiceList = new ArrayList<>();

		try {
			merchantServiceList = mDAO.getMerchantServicesList(Mid);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return merchantServiceList;
	}
	
	@RequestMapping(value = "/getMerchant/{Mid}", method = RequestMethod.GET, headers = "Accept=application/json")
	public MerchantBean getMerchant(@PathVariable String Mid) {
		MerchantDAO mDAO = new MerchantDAO();
		MerchantBean merchantBean = new MerchantBean();

		try {
			merchantBean = mDAO.getMerchantBean(Mid);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return merchantBean;
	}

	@ModelAttribute
	public void setVaryResponseHeader(HttpServletResponse response) {
		response.setHeader("Access-Control-Allow-Origin", "*");
	}
}
