package sabpaisa.portal.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import sabpaisa.portal.model.FeedsBean;
import sabpaisa.portal.model.OrganizationProductBean;
import sabpaisa.portal.model.OrganizationsBean;
import sabpaisa.portal.model.ServicesBean;

@RestController
public class FeedsActionController {
	@Autowired
	sabpaisa.portal.dao.OrganizationDao OrganizationDao;
	@Autowired
	sabpaisa.portal.dao.OrganizationsUseresDao orgUsersDAO;
	@Autowired
	sabpaisa.portal.dao.FeedsDao feedsDAO;
	
	@RequestMapping(value="/setFeedsDetailsByOrganization/{FeedsName}/{organizationName}", method = RequestMethod.GET, headers = "Accept=application/json")
    public String setFeedsDetailsByOrganization(@PathVariable String FeedsName,@PathVariable String organizationName){
		System.out.println("productName::"+FeedsName);
		
		FeedsBean feedBean=new FeedsBean();
		OrganizationsBean orgBean=new OrganizationsBean();
		orgBean=OrganizationDao.getOrganizationID(organizationName);
		//orgBean.setOrganizationId(orgID);
		feedBean.setOrgBean(orgBean);
		
		feedBean.setFeedText(FeedsName);
		
		feedsDAO.saveFeedsDetails(feedBean);
		String status="Value Successfully Saved in DB ";
		return status;
	}
	
	@RequestMapping(value = "/getFeedsDetailsBasedOnOrganizations/{organizationName}", method = RequestMethod.GET, headers = "Accept=application/json")
	public @ResponseBody List<FeedsBean> getFeedsDetailsBasedOnOrganizations(@PathVariable String organizationName){
		System.out.println("Get Organization List::");
		List<FeedsBean> feedsList=null;
		OrganizationsBean orgBean=new OrganizationsBean();
		orgBean=OrganizationDao.getOrganizationID(organizationName);
		Integer orgId=orgBean.getOrganizationId();
		feedsList=feedsDAO.getOrgProductList(orgId);
		/*for (OrganizationsBean organizationBean:organizationList) {
			System.out.println("Organization List SIze :::"+organizationList.size());
		}*/
		return feedsList;
		}
	 
}
