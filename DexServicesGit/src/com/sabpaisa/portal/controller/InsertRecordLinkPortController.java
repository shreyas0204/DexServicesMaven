package sabpaisa.portal.controller;

import java.sql.SQLException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class InsertRecordLinkPortController {
	static Logger log = Logger.getLogger(InsertRecordLinkPortController.class);

	@Autowired
	sabpaisa.portal.dao.LinkPortDAO linkPortDAO;

	@RequestMapping(value = "/setLinkPortDetails/{Mid}/{PayerFName}/{PayerLName}/{Email}/{Mobile}/{Amount}/{Address}", method = RequestMethod.GET, headers = "Accept=application/json")
	public String setLinkPortDetails(@PathVariable String Mid, @PathVariable String PayerFName,
			@PathVariable String PayerLName, @PathVariable String Email, @PathVariable String Mobile,
			@PathVariable String Amount, @PathVariable String Address) {
		log.info("OrganizationName :::::" + PayerFName);
		log.info("Type :::::" + Mobile);
		Boolean dbstatus = false;
		try {
			dbstatus = linkPortDAO.saveLinkPortPayerData(Mid, PayerFName, PayerLName, Email, Mobile, Amount, Address);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String status = dbstatus.toString();
		return status;
	}
}
