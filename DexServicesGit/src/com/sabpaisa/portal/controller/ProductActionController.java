package sabpaisa.portal.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import sabpaisa.portal.model.OrganizationProductBean;
import sabpaisa.portal.model.OrganizationsBean;

@RestController
public class ProductActionController {

	@Autowired
	sabpaisa.portal.dao.OrganizationProductDao orgProductDao;
	@Autowired
	sabpaisa.portal.dao.OrganizationDao OrganizationDao;
	
	@RequestMapping(value = "/getProductDetails", method = RequestMethod.GET, headers = "Accept=application/json")
	public @ResponseBody List<OrganizationProductBean> getProductDetails(){
		System.out.println("Get Organization List::");
		List<OrganizationProductBean> productList=null;
		try {
			productList=orgProductDao.getProductList();
		} catch (Exception e) {
		 System.out.println("In catch Block :::::::::");
		 e.printStackTrace();
		}
		
		
		return productList;
		}
	
	@RequestMapping(value = "/getProductDetailsBasedOnOrganizations/{organizationName}", method = RequestMethod.GET, headers = "Accept=application/json")
	public @ResponseBody List<OrganizationProductBean> getProductDetailsBasedOnOrganizations(@PathVariable String organizationName){
		System.out.println("Get Organization List::");
		//OrganizationProductBean orgProductBean=null;
		List<OrganizationProductBean> productList=null;
		OrganizationsBean orgBean=new OrganizationsBean();
		orgBean=OrganizationDao.getOrganizationID(organizationName);
		Integer orgId=orgBean.getOrganizationId();
		productList=orgProductDao.getOrgProductList(orgId);
		/*
		for (OrganizationProductBean orgProductBean:productList) {
			System.out.println("Organization List SIze :::"+orgProductBean.getProductdesctiption());
			
			
		}
		*/
		return productList;
		}
	
	@RequestMapping(value="/setProductDetails/{productName}/{organizationName}/{productType}", method = RequestMethod.GET, headers = "Accept=application/json")
	public String setProductDetails(@PathVariable String productName,@PathVariable String organizationName,@PathVariable String productType){
		OrganizationProductBean productBean=new OrganizationProductBean();
		OrganizationsBean orgBean=new OrganizationsBean();
		orgBean=OrganizationDao.getOrganizationID(organizationName);
		productBean.setProductdesctiption(productName);
		productBean.setProductType(productType);
       // productBean.setOrgBean(orgBean);
		try {
			orgProductDao.saveProductDetails(productBean);
			String status="Successfully Saved in DB";
			return status;
		} catch (Exception e) {
			e.printStackTrace();
			String status="Sorry";
			return status;
		}
				
	}
	
	// Get Product Details Based on Organization and User
	/*@RequestMapping(value="/getProductBasedonUser/{UserID}",method=RequestMethod.GET, headers = "Accept=application/json")
	public @ResponseBody List<OrganizationProductBean>getProductBasedonUser(@PathVariable Integer UserID){
		List<OrganizationProductBean> oregUsersBean=null;
		System.out.println("In side the getOrganizationBasedonUser::");
		oregUsersBean=orgProductDao.getProductUsers(UserID);
	//String	response="{\"Result:::\":"+oregUsersBean+"}";
		return "{\"Result:::\":"+oregUsersBean+"}";
	}*/
	
	//END
	
	@RequestMapping(value="/getProductBasedonUser/{UserID}",method=RequestMethod.GET, headers = "Accept=application/json")
	public @ResponseBody String getProductBasedonUser(@PathVariable Integer UserID){
		List<OrganizationProductBean> oregUsersBean=null;
		System.out.println("In side the getOrganizationBasedonUser::");
		oregUsersBean=orgProductDao.getProductUsers(UserID);
	//String	response="{\"Result:::\":"+oregUsersBean+"}";
		return "{\"Result:::\":"+oregUsersBean+"}";
	}
}
