package sabpaisa.portal.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import sabpaisa.portal.model.FeaturesAccessBean;
import sabpaisa.portal.model.OrganizationProductBean;

@RestController
public class FeaturesAccessActionController {

	@Autowired
	sabpaisa.portal.dao.FeaturesAccessDao featureAccessDAO;
	
	@RequestMapping(value = "/getAllFeaturesDetails", method = RequestMethod.GET, headers = "Accept=application/json")
	public @ResponseBody List<FeaturesAccessBean> getAllFeaturesDetails(){
		System.out.println("Get Organization List::");
		List<FeaturesAccessBean> productList=null;
		productList=featureAccessDAO.getProductList();
		/*for (OrganizationsBean organizationBean:organizationList) {
			System.out.println("Organization List SIze :::"+organizationList.size());
		}*/
		return productList;
		}
	
	@RequestMapping(value = "/getFeaturesDetailsBasedUserAndProduct/{userID}/{productID}", method = RequestMethod.GET, headers = "Accept=application/json")
	public @ResponseBody List<FeaturesAccessBean> getFeaturesDetailsBasedUserAndProduct(@PathVariable Integer userID,
			@PathVariable Integer productID){
		System.out.println("Get Organization List::");
		List<FeaturesAccessBean> productList=null;
		productList=featureAccessDAO.getProductListBasedOnCriteria(userID,productID);
		/*for (OrganizationsBean organizationBean:organizationList) {
			System.out.println("Organization List SIze :::"+organizationList.size());
		}*/
		return productList;
		}
	
	@RequestMapping(value = "/getFeaturesDetailsBasedProduct/{productID}", method = RequestMethod.GET, headers = "Accept=application/json")
	public @ResponseBody List<FeaturesAccessBean> getFeaturesDetailsBasedProduct(@PathVariable Integer productID){
		System.out.println("Get Organization List::");
		List<FeaturesAccessBean> productList=null;
		productList=featureAccessDAO.getProductListBasedOnProduct(productID);
		/*for (OrganizationsBean organizationBean:organizationList) {
			System.out.println("Organization List SIze :::"+organizationList.size());
		}*/
		return productList;
		}
}
