package sabpaisa.portal.controller;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import sabpaisa.portal.enPasswordGenerator.PasswordEncryption;
import sabpaisa.portal.model.LoginBean;
import sabpaisa.portal.model.OrganizationsBean;
import sabpaisa.portal.model.UsersBean;

@RestController
public class UsersActionController {
	@Autowired
	sabpaisa.portal.dao.UsersDao userDAO;
	@Autowired
	sabpaisa.portal.dao.LoginDao loginDAO;
	@Autowired
	sabpaisa.portal.dao.OrganizationDao OrganizationDao;

	@RequestMapping(value = "/getUserDetails", method = RequestMethod.GET, headers = "Accept=application/json")
	public @ResponseBody List<UsersBean> getProductDetails() {
		System.out.println("Get Organization List::");
		List<UsersBean> productList = null;
		productList = userDAO.getProductList();
		/*
		 * for (OrganizationsBean organizationBean:organizationList) {
		 * System.out
		 * .println("Organization List SIze :::"+organizationList.size()); }
		 */
		return productList;
	}

	@RequestMapping(value = "/getUserBasedOnOrganizations/{partnerId}", method = RequestMethod.GET, headers = "Accept=application/json")
	public @ResponseBody List<UsersBean> getUserBasedOnOrganizations(@PathVariable Integer partnerId) {
System.out.println("Inside the Controller");
		List<UsersBean> partnerDetailBeanlst = null;
		partnerDetailBeanlst = userDAO.getRegisteredStudent(partnerId);
		// partnerDetailBeanlst = PartnerDao.getPartnerDetailsById(partnerID);

		return partnerDetailBeanlst;
	}
	
	@RequestMapping(value = "/getUserProfileByID/{userId}", method = RequestMethod.GET, headers = "Accept=application/json")
	public @ResponseBody List<UsersBean> getUserProfileByID(@PathVariable String userId) {
		String status="SUCCESS";
		System.out.println("Inside the Controller");
		UsersBean userbean = new UsersBean();
		List<UsersBean> userDetailBeanlst = null;
		userDetailBeanlst = userDAO.getUserProfile(userId);
		
		return userDetailBeanlst;
	}
	
	// Start
	@RequestMapping(value = "/getUserBeanRegistereUser/{UserName}/{Password}/{FirstName}/{LastName}/{Address}/{Contact}/{Email}/{organizationName}", method = RequestMethod.GET)
public String getUserBeanRegistereUser(@PathVariable String UserName,@PathVariable String Password,
		@PathVariable String FirstName,@PathVariable String LastName,@PathVariable String Address,@PathVariable String Contact,@PathVariable String Email,@PathVariable String organizationName) throws InvalidKeyException, NoSuchAlgorithmException, InvalidKeySpecException, InvalidAlgorithmParameterException, UnsupportedEncodingException, IllegalBlockSizeException, BadPaddingException{
		
		PasswordEncryption.encrypt(Password);
		String encryptedPwd = PasswordEncryption.encStr;
		System.out.println("Encripted Password ::::::::"+encryptedPwd);
		String status="";
		try {
			OrganizationsBean orgBean=new OrganizationsBean();
			orgBean=OrganizationDao.getOrganizationID(organizationName);
			LoginBean loginbean=new LoginBean();
			loginbean.setPassword(encryptedPwd);
			loginbean.setStatus("pending");
			//orgBean.setOrganizationId(organizationID);
			loginbean.setUserName(UserName);
			UsersBean userbean=new UsersBean();
			userbean.setLastName(LastName);
			userbean.setFirstName(FirstName);
			userbean.setAddress(Address);
			userbean.setContactNumber(Contact);
			userbean.setEmailID(Email);
			loginbean.setUserBean(userbean);
			userbean.setLoginBean(loginbean);
			
			userbean.setOrgBean(orgBean);
			int reUserID=userDAO.saveUserDetails(userbean);
			int orguser=userDAO.saveOrganizationUser(orgBean,userbean);
			System.out.println("Controlleer User IDDD::::::::"+reUserID);
			status="Saved Successfully";
			return status;
		} catch (Exception e) {
			e.printStackTrace();
			status="Not Saved";
			return status;
		}
	}
	
	//END
	
	//Get User Based on FEED 
	/*@RequestMapping(value = "/getUserBeanRegistereUser/{UserName}/{Password}/{FirstName}/{LastName}/{Address}/{Contact}/{Email}/{organizationName}", method = RequestMethod.GET)
*/
	//END
	
	// Start
	/*@RequestMapping(value = "/getUserApprovalStatus/{UserID}", method = RequestMethod.GET)
	public String getUserBeanRegistereUser(@PathVariable String UserID){
		
		PasswordEncryption.encrypt(Password);
		String encryptedPwd = PasswordEncryption.encStr;
		System.out.println("Encripted Password ::::::::"+encryptedPwd);
		String status="";
		try {
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		return null;
	}*/

	//END
	@ModelAttribute
	public void setVaryResponseHeader(HttpServletResponse response) {
	    response.setHeader("Access-Control-Allow-Origin", "*");
	}
}
