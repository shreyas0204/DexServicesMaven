package sabpaisa.portal.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import sabpaisa.portal.model.GroupBean;
import sabpaisa.portal.model.OrganizationsBean;
import sabpaisa.portal.model.OrganizationsUsersBean;
import sabpaisa.portal.model.RolesBean;

@RestController
public class GroupsActionController {

	@Autowired
	sabpaisa.portal.dao.GroupDao groupsDAO;
	@Autowired
	sabpaisa.portal.dao.OrganizationDao OrganizationDao;
	
	@RequestMapping(value="/saveGroups/{GroupName}/{groupType}/{organizationName}",method=RequestMethod.GET, headers = "Accept=application/json")
	public String saveGroups(@PathVariable String GroupName,@PathVariable String groupType,@PathVariable String organizationName){
		
		OrganizationsBean orgBean=new OrganizationsBean();
		RolesBean roleBean=new RolesBean();
		GroupBean groupBean=new GroupBean();
		orgBean=OrganizationDao.getOrganizationID(organizationName);
		System.out.println("Organization Bean :::" +orgBean);
		List<GroupBean> groupsBean=null;	
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		System.out.println(dateFormat.format(date));
		String created_Date=dateFormat.format(date);		
		System.out.println("Assign in variable::::::" +created_Date);		
		/*roleBean.setRole_Name(RolesName);
		roleBean.setRole_Descriptions(Description);
		roleBean.setOrgBean(orgBean);*/
		groupBean.setGroupName(GroupName);
		groupBean.setGroupType(groupType);
		groupBean.setCreated_Date(created_Date);
		groupBean.setOrgBean(orgBean);
		String statusCode=null;
		try {
			int setList=groupsDAO.saveCreatedGroups(groupBean);
			System.out.println("Save list :::" +setList);
			
					return "{\"status\":\""+setList+"\" , \"StatusCode\":\""+statusCode+"\"}";
		} catch (Exception e) {
			e.printStackTrace();
			String status="Sorry";
			return status;
		}		
	} 
	
	//Get All Groups
	@RequestMapping(value="/getAllGroups",method=RequestMethod.GET, headers = "Accept=application/json")
	public @ResponseBody List<GroupBean>getAllGroups(){
		List<GroupBean> oregBean=null;
		System.out.println("In side the getOrganizationBasedonUser::");
		oregBean=groupsDAO.getAllGroups();
		return oregBean;
	}
	//END
	//Get Organization Based on User ID
	@RequestMapping(value="/getGroupsBasedonOrg/{organizationName}",method=RequestMethod.GET, headers = "Accept=application/json")
	public @ResponseBody List<GroupBean>getGroupsBasedonOrg(@PathVariable String organizationName){
		List<GroupBean> groupBean=null;
		System.out.println("In side the getOrganizationBasedonUser::");
		OrganizationsBean orgBean=new OrganizationsBean();
		orgBean=OrganizationDao.getOrganizationID(organizationName);
		Integer orgId=orgBean.getOrganizationId();
		
		groupBean=groupsDAO.getGroupsUsers(orgId);
		return groupBean;
	}
	//END
}
