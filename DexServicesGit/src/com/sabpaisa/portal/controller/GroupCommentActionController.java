package sabpaisa.portal.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import sabpaisa.portal.model.GroupBean;
import sabpaisa.portal.model.GroupCommentsBean;

@RestController
public class GroupCommentActionController {

	@Autowired
	sabpaisa.portal.dao.GroupMembersDao groupMembersDAO;
	@Autowired
	sabpaisa.portal.dao.OrganizationDao OrganizationDao;
	@Autowired
	sabpaisa.portal.dao.GroupCommentDao grpCommntDAO;
	
	//Get Comment Based on UserID
	@RequestMapping(value="/getCommentsBasedOnuserID/{UserId}",method=RequestMethod.GET, headers = "Accept=application/json")
	public @ResponseBody List<GroupCommentsBean>getCommentsBasedOnuserID(@PathVariable Integer UserId){
		List<GroupBean> oregBean=null;
		List<GroupCommentsBean> commentBean=null;
		commentBean=grpCommntDAO.getMessageBasedOnUserID(UserId);
		System.out.println("Return Bean Size ::::"+commentBean.size());
		return commentBean;
	}
	//End
}
