package sabpaisa.portal.controller;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;

import org.apache.log4j.Logger;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;



















import sabpaisa.portal.dao.OrganizationsUseresDao;
import sabpaisa.portal.model.OrganizationsBean;
import sabpaisa.portal.model.OrganizationsUsersBean;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@RestController
public class OrganizationActionController {
	static Logger log = Logger.getLogger(OrganizationActionController.class);
	@Autowired
	sabpaisa.portal.dao.OrganizationDao OrganizationDao;
	@Autowired
	sabpaisa.portal.dao.OrganizationsUseresDao orgUsersDAO;
	@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)

	/*<--------------------------------Fetch all record------------------------------->*/
	
	/*@RequestMapping(value = "/addOrganization", method = RequestMethod.POST, headers = "Accept=application/json")
	public String addOrganization(@RequestBody OrganizationsBean organizationBean)throws InvalidKeyException,
	NoSuchAlgorithmException, InvalidKeySpecException, InvalidAlgorithmParameterException,
	UnsupportedEncodingException, IllegalBlockSizeException, BadPaddingException{
		String status="";
		System.out.println("In s ie the add organization ::::::::: saving :::::::::::");
		organizationBean.setOrganizationId(1);
		organizationBean.setOrganization_name("Dexpert");
		organizationBean.setRelationship_type("SELF");
		if(OrganizationDao.saveOrganization(organizationBean)){
			System.out.println("in Save organization in the if condition::::::::::");
			status="success";
		}
		else {
			status="fail";
		}
		return status;
	}*/
	
	// Organizations Details :::
	@RequestMapping(value = "/getOrganizationDetails", method = RequestMethod.GET, headers = "Accept=application/json")
	public @ResponseBody List<OrganizationsBean> getOrganizationDetails(){
		System.out.println("Get Organization List::");
		List<OrganizationsBean> organizationList=null;
		///Hibernate.initialize(OrganizationDao.getOrganizationList());
		organizationList=OrganizationDao.getOrganizationList();
		
		for (OrganizationsBean organizationBean:organizationList) {
			
			System.out.println("Organization Name :::"+organizationBean.getOrganization_name());
			 
		}
		return organizationList;
		}
@RequestMapping(value="/setOrganizationDetails/{OrganizationName}/{Type}",method=RequestMethod.GET, headers = "Accept=application/json")
public String setOrganizationDetails(@PathVariable String OrganizationName,@PathVariable String Type){
	System.out.println("OrganizationName :::::"+OrganizationName);
	System.out.println("Type :::::"+Type);
	OrganizationsBean orgBean=new OrganizationsBean();
	orgBean.setOrganization_name(OrganizationName);
	orgBean.setRelationship_type(Type);
     Integer id=OrganizationDao.saveOrganizations(orgBean);
     System.out.println("return ID :::: " +id);
	String status="Successfully Saved in DB";
	return status;
} 

//Get Organization Based on User ID
@RequestMapping(value="/getOrganizationBasedonUser/{UserID}",method=RequestMethod.GET, headers = "Accept=application/json")
public @ResponseBody List<OrganizationsBean> getOrganizationBasedonUser(@PathVariable Integer UserID){
	List<OrganizationsBean> oregUsersBean=null;
	System.out.println("Inside the getOrganizationBasedonUser:: \nGetting Organisations for userId "+UserID);
	//String ProdListJson="";
	//ProdListJson=orgUsersDAO.getOrganizationUsers(UserID);
	oregUsersBean=orgUsersDAO.getOrganizationUsers(UserID);
	
	return oregUsersBean;
}
//END

// Random Password Generation 
@RequestMapping(value="/RandomPasswordGeneration/{UserID}",method=RequestMethod.GET, headers = "Accept=application/json")
public String RandomPasswordGeneration(@PathVariable Integer UserID){
	Random random = new Random((new Date()).getTime());
	
	int length=0;
	char[] values = {'a','b','c','d','e','f','g','h','i','j',
            'k','l','m','n','o','p','q','r','s','t',
            'u','v','w','x','y','z','0','1','2','3',
            '4','5','6','7','8','9'};

   String out = "";

   for (int i=0;i<length;i++) {
       int idx=random.nextInt(values.length);
       out += values[idx];
      
   }
   System.out.println("Random Password::::::" +out);
   return out;
   
}

// END
@ModelAttribute
public void setVaryResponseHeader(HttpServletResponse response) {
    response.setHeader("Access-Control-Allow-Origin", "*");
}

}
