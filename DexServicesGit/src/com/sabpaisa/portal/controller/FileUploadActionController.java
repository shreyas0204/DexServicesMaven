package sabpaisa.portal.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.DefaultHandlerExceptionResolver;

import sabpaisa.portal.model.FileUploadBean;

//@RestController
public class FileUploadActionController<MultipartFormDataInput> extends DefaultHandlerExceptionResolver {

	@Autowired
	sabpaisa.portal.dao.FileUploadDao fileUploadDAO;

    @Value("#{servletContext.contextPath}")
    private String servletContextPath;
    private static final String UPLOAD_DIRECTORY ="/images";
	
    
    @RequestMapping(value="/saveFileData/{fileName}/{fileData}",method=RequestMethod.GET, headers = "Accept=application/json")
    public String saveFileData(@PathVariable String fileName,@PathVariable byte[] fileData){
    	System.out.println("In side the save file data :::::::::::::::");
    	FileUploadBean fileBean=new FileUploadBean();
    	fileBean.setFileName(fileName);
    	fileBean.setFileData(fileData);
    	int result=fileUploadDAO.saveFilerecord(fileBean);
    	return null;
    }
    
    //Shubham's code
    
    
    
 /* @RequestMapping(value="/saveFileData/{fileName}",method=RequestMethod.POST, headers = "Accept=application/json")
    public String saveFileDataNew(@RequestParam("file") MultipartFile InputPart){
    	System.out.println("In side the save file data :::::::::::::::");
    	Map<String, List<InputPart>> uploadForm = input.getFormDataMap();
    	FileUploadBean fileBean=new FileUploadBean();
    	fileBean.setFileName(fileName);
    	fileBean.setFileData(fileData);
    	int result=fileUploadDAO.saveFilerecord(fileBean);
    	return null;
    }*/
  
///////////////////////Please uncomment following to get the error -shubham////////////
    /*  
  @RequestMapping(value="urlvale/{}/{}",method=RequestMethod.POST, consumes="multipart/form-data")
  public Response uploadFile(MultipartFormDataInput  input) throws IOException {
	  
      Map<String, List<InputPart>> uploadForm = input.getFormDataMap();
    	//input.getFormDataMap();

      // Get file data to save
      List<InputPart> inputParts = uploadForm.get("attachment");

      for (InputPart inputPart : inputParts) {
          try {

        	  MultivaluedMap<String, String> header = inputPart.getHeaders();
              String fileName = getFileName(header);
 
              // convert the uploaded file to inputstream
              InputStream inputStream = (InputStream) inputPart.getBody(InputStream.class, null);

              byte[] bytes = IOUtils.toByteArray(inputStream);
              // constructs upload file path
              fileName = "/home/user/Downloads/" + fileName;
              writeFile(bytes, fileName);

                
              return Response.status(200).entity("Uploaded file name : " + fileName)
                      .build();

          } catch (Exception e) {
              e.printStackTrace();
          }
      }
      return null;
  }
  */
    
  
  /*private String getFileName(MultivaluedMap<String, String> header) {
	  
      String[] contentDisposition = header.getFirst("Content-Disposition").split(";");

      for (String filename : contentDisposition) {
          if ((filename.trim().startsWith("filename"))) {

              String[] name = filename.split("=");

              String finalFileName = name[1].trim().replaceAll("\"", "");
              return finalFileName;
          }
      }
      return "unknown";
  }*/

  
 /** * @author Aditya
  * @date 03/15/2017 */
@RequestMapping(value="/upload", method=RequestMethod.POST)
  public @ResponseBody String handleFileUpload( 
          @RequestParam("file") MultipartFile file){
          String name = "test11";
      if (!file.isEmpty()) {
          try {
              byte[] bytes = file.getBytes();
              BufferedOutputStream stream = 
                      new BufferedOutputStream(new FileOutputStream(new File(name + "-uploaded")));
              stream.write(bytes);
              stream.close();
              return "Upload Successful New Name: " + name + "-uploaded !";
          } catch (Exception e) {
              return "Upload Failed Due To" + e.getMessage();
          }
      } else {
          return "UPload Failed";
      }
  }
  
  // Utility method
  private void writeFile(byte[] content, String filename) throws IOException {
      File file = new File(filename);
      if (!file.exists()) {
          System.out.println("not exist> " + file.getAbsolutePath());
          file.createNewFile();
      }
      FileOutputStream fop = new FileOutputStream(file);
      fop.write(content);
      fop.flush();
      fop.close();
  }
    
//Shubham's code
    
    
	@RequestMapping(value = "/savefile", method = RequestMethod.POST)
	public ModelAndView upload(@RequestParam CommonsMultipartFile file, HttpSession session) {
		FileUploadBean fileUploadBean = new FileUploadBean();

		String path = session.getServletContext().getRealPath("/");
		String filename = file.getOriginalFilename();
		  

		System.out.println(path + " " + filename);
		try {

			File fileToCreate = new File(path, filename);
			byte[] bFile = new byte[(int) fileToCreate.length()];

			FileInputStream fileInputStream = new FileInputStream(fileToCreate);
			fileInputStream.read(bFile);
			fileInputStream.close();
			fileUploadBean.setFileData(bFile);

			System.out.println("Saving file: " + file.getOriginalFilename());

			fileUploadBean.setFileName(file.getOriginalFilename());
			// / fileUploadBean.setFileData(file.getBytes());

			fileUploadDAO.save(fileUploadBean);

		} catch (Exception e) {
			System.out.println(e);
		}
		return new ModelAndView("upload-success", "filename", path + "/" + filename);
	}

	@RequestMapping(value = "/Ggggggggg", method = RequestMethod.GET)
	public ModelAndView Ggggggggg(HttpServletRequest request) {
		System.out.println("In side the Upload File Method:::::::::::::::::");
		return new ModelAndView("FileUpload");
	}

	
	  /*@RequestMapping(value = "/doUploadFile", method = RequestMethod.POST)
	  public String doUploadFile(HttpServletRequest request,
	  
	  @RequestParam CommonsMultipartFile[] fileUpload) throws Exception{
	 System.out.println("Get Organization List::"); String status=null; if
	 (fileUpload != null && fileUpload.length > 0) { for (CommonsMultipartFile
	 aFile : fileUpload){
	
	  System.out.println("Saving file: " + aFile.getOriginalFilename());
	  FileUploadBean fileUploadBean=new FileUploadBean();
	  
	  fileUploadBean.setFileName(aFile.getOriginalFilename());
	  fileUploadBean.setFileData(aFile.getBytes());
	  
	  fileUploadDAO.save(fileUploadBean); } 
	 }
	
	  }*/
	
	@RequestMapping(value="/upload", method=RequestMethod.POST)
    public @ResponseBody String upload( 
            @RequestParam("file") MultipartFile file, HttpSession session){
		/*FileUploadBean fileUploadBean = new FileUploadBean();

		//ServletConfig config = getServletConfig();
		//String path = servletContextPath();
		//String path = session.getServletContext().getRealPath("/");
		//String path = servletContextPath;
		String filename = file.getOriginalFilename();
		File fileToCreate = new File("C:\\", filename);
            String name = "test11";
            System.out.println("inside the sir tukka::::::::::::::: ");
            ServletContext context = session.getServletContext();  
            String path = context.getRealPath(UPLOAD_DIRECTORY);  
           // String filename = file.getOriginalFilename();  
          
            System.out.println(path+" "+filename);        
          
            
        if (!file.isEmpty()) {
        	
            try {
                byte[] bytes = file.getBytes();
              //  BufferedOutputStream stream = 
                //        new BufferedOutputStream(new FileOutputStream(new File(name + "-uploaded")));
                BufferedOutputStream stream =new BufferedOutputStream(new FileOutputStream(  
                        new File(path + File.separator + filename)));  
                stream.write(bytes);
                stream.flush(); 
                stream.close();
                return "You successfully uploaded " + name + " into " + name + "-uploaded !";
            } catch (Exception e) {
            	e.printStackTrace();
                return "You failed to upload " + name + " => " + e.getMessage();
            }
        	
            try {
                byte[] bytes = file.getBytes();
                System.out.println("step1");
                BufferedOutputStream stream = 
                        new BufferedOutputStream(new FileOutputStream(new File(filename + "-uploaded")));
                stream.write(bytes);
                System.out.println("step2");
                FileInputStream fileInputStream = new FileInputStream(fileToCreate);
    			fileInputStream.read(bytes);
    			fileInputStream.close();
    			fileUploadBean.setFileData(bytes);

    			System.out.println("Saving file: " + file.getOriginalFilename());

    			fileUploadBean.setFileName(file.getOriginalFilename());
    			// / fileUploadBean.setFileData(file.getBytes());

    			fileUploadDAO.save(fileUploadBean);
                stream.close();
                return "You successfully uploaded " + name + " into " + name + "-uploaded !";
            } catch (Exception e) {
            	                return "You failed to upload " + name + " => " + e.getMessage();
            }
        } else {
            return "You failed to upload " + name + " because the file was empty.";
        }
    }*/
		
		
	
		String path=session.getServletContext().getRealPath("/");  
        String filename=file.getOriginalFilename();  
          
        System.out.println(path+" "+filename);  
        try{  
        byte barr[]=file.getBytes();  
          
        BufferedOutputStream bout=new BufferedOutputStream(  
                 new FileOutputStream(path+"/"+filename));  
        bout.write(barr);  
       
        		  System.out.println("Saving file: " + file.getOriginalFilename());
        		  FileUploadBean fileUploadBean=new FileUploadBean();
        		  
        		  fileUploadBean.setFileName(file.getOriginalFilename());
        		  fileUploadBean.setFileData(barr);
        		  
        		  fileUploadDAO.save(fileUploadBean); 
  	  
  	  
        bout.flush();  
        bout.close();  
        return "You successfully uploaded ";
        
        }
        catch(Exception e){System.out.println(e);
        return "You Filed uploaded ";
        
        }  
        }
        
	
}
