package sabpaisa.portal.controller;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import payplatter.dao.LoginDAO;
import sabpaisa.portal.dao.OtpDao;
import sabpaisa.portal.enPasswordGenerator.PasswordEncryption;
import sabpaisa.portal.enPasswordGenerator.RandomPasswordGenerator;
import sabpaisa.portal.model.LoginBean;
import sabpaisa.portal.model.OtpBean;
import sabpaisa.portal.model.UsersBean;
import sabpaisa.portal.utilityclasses.ExternalProperties;
import sabpaisa.portal.utilityclasses.GeneralUtilityClass;
import sabpaisa.portal.utilityclasses.Notifications;

@RestController
public class LoginActionController {
	static Logger log = Logger.getLogger(LoginActionController.class);
	@Autowired
	sabpaisa.portal.dao.LoginDao loginDAO;

	private LoginBean loginBean = new LoginBean();
	@Autowired
	sabpaisa.portal.dao.OtpDao otpdao;
	/*
	 * ExternalProperties extProperties = new ExternalProperties(); Properties
	 * properties = extProperties.getPropValues();
	 */

	@Autowired
	sabpaisa.portal.dao.OrganizationDao OrganizationDao;

	//

	@RequestMapping(value = "/validateLogindetails/{userName}/{password}", method = RequestMethod.GET, headers = "Accept=application/json")
	public String validateLogindetails(@PathVariable String userName, @PathVariable String password)
			throws InvalidKeyException, NoSuchAlgorithmException, InvalidKeySpecException,
			InvalidAlgorithmParameterException, UnsupportedEncodingException, IllegalBlockSizeException,
			BadPaddingException {
		List<UsersBean> userbean = null;
		System.out.println("Password :::" + password);

		PasswordEncryption.encrypt(password);

		String encryptedPwd = PasswordEncryption.encStr;
		String statusCode = null;
		String statusmsg = null;
		System.out.println("encripted Passw ::::::" + encryptedPwd);
		try {
			loginBean = loginDAO.validateLoginCredentialsDao(userName, encryptedPwd);
			System.out.println("user Id :::::::" + loginBean.getUserName());
			loginBean.getStatus();

			System.out.println("User Id  ravi ::::::::::::::" + loginBean.getUserBean().getUserId());
			// System.out.println("User ID :::" +userbean.get(0));
			String orgID = "2";
			if (loginBean.getStatus().contentEquals("approved")) {
				System.out.println("In side the if condition ::::::::::::::::");
				statusCode = "0001";

			} else {
				statusmsg = "Login Successfully Done!!!!!!";
				System.out.println("in side the else :::::::::::::::::");
				statusCode = "0000";
			}
			return "{\"status\":\"" + loginBean.getStatus() + "\" , \"LoginMSG\":\"" + statusmsg
					+ "\" ,\"StatusCode\":\"" + statusCode + "\" , \"UserID\":\"" + loginBean.getUserBean().getUserId()
					+ "\" }";
		} catch (Exception e) {
			e.printStackTrace();
			statusCode = "0003";
			String status = "Login Failed";
			statusmsg = "UserID or Password doesn't match!!!!!!!!!!!!";
			// return loginBean;
			return "{\"status\":\"" + status + "\",\"StatusCode\":\"" + statusCode + "\",\"LoginMSG\":\"" + statusmsg
					+ "\", \"User doesn't exist\"}";
		}
		// loginBean = loginDAO.validateLoginCredentialsDao(userName,
		// encryptedPwd);
		// return "Success";
	}

	/*
	 * @RequestMapping(value = "/getOrganizationDetails", method =
	 * RequestMethod.GET, headers = "Accept=application/json") public
	 * 
	 * @ResponseBody List<OrganizationsBean> getOrganizationDetails(){
	 * System.out.println("Get Organization List::"); List<OrganizationsBean>
	 * organizationList=null;
	 * ///Hibernate.initialize(OrganizationDao.getOrganizationList());
	 * organizationList=OrganizationDao.getOrganizationList();
	 * 
	 * for (OrganizationsBean organizationBean:organizationList) {
	 * 
	 * System.out.println("Organization Name :::"+organizationBean.
	 * getOrganization_name());
	 * 
	 * } return organizationList; }
	 */

	/*
	 * @RequestMapping(value = "/validateLogin", method = RequestMethod.GET,
	 * headers = "Accept=application/json") public List<LoginBean>
	 * validateLoginCredentials(@RequestBody LoginBean loginBean) throws
	 * InvalidKeyException, NoSuchAlgorithmException, InvalidKeySpecException,
	 * InvalidAlgorithmParameterException, UnsupportedEncodingException,
	 * IllegalBlockSizeException, BadPaddingException { //List<LoginBean>
	 * loginBean = null; loginBean.getUserName(); loginBean.getPassword();
	 * System.out.println("User Name ::::::::::" +loginBean.getUserName());
	 * //PasswordEncryption.encrypt(password); return null;
	 * 
	 * String encryptedPwd = PasswordEncryption.encStr; try { loginBean =
	 * loginDAO.validateLoginCredentialsDao(userName, encryptedPwd); return
	 * loginBean; } catch (Exception e) { e.printStackTrace(); String status=
	 * "Login Failed"; return loginBean; } loginBean =
	 * loginDAO.validateLoginCredentialsDao(userName, encryptedPwd); return
	 * loginBean; }
	 */

	@RequestMapping(value = "/Loginpage", method = RequestMethod.GET)
	public ModelAndView Loginpage(HttpServletRequest request) {
		System.out.println("In side the Upload File Method:::::::::::::::::");
		return new ModelAndView("login");
	}

	/**
	 * @author Aditya Mar 10, 2017
	 */
	@RequestMapping(value = "/SendOTP/{mobile_no}")
	public String SendOtp(@PathVariable String mobile_no) {
		log.info("Generating OTP for " + mobile_no);
		String status = "";
		try {
			OtpBean otpbean = new OtpBean();
			// Generate OTP using Password Generator method
			String otp = RandomPasswordGenerator.generatePswd(6, 6, 0, 6, 0);
			log.info("OTP generated " + otp);
			if (GeneralUtilityClass.isInteger(mobile_no, 10)) {// Check if
																// mobile number
																// is valid
				log.info("Is a valid Mobile number");
				// Delete previous OTP for same mobile number if any
				otpdao.deleteOtpForNumber(mobile_no);
				otpbean.setMobile_no(mobile_no);
				otpbean.setOtp(otp);
				otpbean.setOtp_generated_date(new Date());
				otpbean.setIsExpired("N");

				otpbean = otpdao.saveOtp(otpbean);
				if (otpbean.getOtp_id() != null) {
					// Send SMS to mobile number here!
					Notifications.sendSMS(mobile_no, "Please use this OTP to log sign up for SabPaisaPay App :" + otp);
					status = "success";
					return "{\"status\":\"" + status + "\", \"response\":\"OTP sent\"}";
				} else {
					status = "failure";
					return "{\"status\":\"" + status + "\", \"response\":\"OTP send error\"}";
				}
			} else {
				status = "incorrect mobile number";
				log.info("Mobile Number not a integer or not of 10 digits");
				return "{\"status\":\"" + status + "\", \"response\":\"OTP send error\"}";
			}
		} catch (Exception e) {
			status = "internal server error";
			e.printStackTrace();
			return "{\"status\":\"" + status + "\", \"response\":\"OTP send error\"}";
		}
	}

	@RequestMapping(value = "/verifyOtp/{OTP}/{mobile_no}")
	public String verifyOTP(@PathVariable String OTP, @PathVariable String mobile_no) {
		OtpBean otpbean = new OtpBean();
		Integer otpExpiry = 15;
		String expireOtpOnHit = "N";
		String status = "";
		log.info("Verifying OTP " + OTP + " for mobile number " + mobile_no);
		try {
			// Check for otp and mobile number combination in db
			otpbean = otpdao.getOtp(OTP, mobile_no);
			if (otpbean.getOtp_id() == null) {
				status = "failure";
				return "{\"status\":\"" + status + "\", \"response\":\"OTP send error\"}";
				// return false;
			}

			// Check for expiry
			try {
				otpExpiry = 15;
				// otpExpiry =
				// Integer.parseInt(properties.getProperty("otpExpiryTime"));
			} catch (Exception e) {
				log.info("Cannot get OTP expiry time from properties file.. defaulting to 15 mins");
				e.printStackTrace();
				otpExpiry = 15;
			}
			Calendar cal = Calendar.getInstance();
			cal.setTime(otpbean.getOtp_generated_date());
			cal.add(Calendar.MINUTE, otpExpiry);
			Date expiryDate = cal.getTime();
			Date currentDate = new Date();
			if (currentDate.after(expiryDate)) {
				log.info("The Otp Date " + otpbean.getOtp_generated_date() + " has gone past the expiry date "
						+ expiryDate + " " + otpExpiry + " mins after current Time " + currentDate);
				status = "failure";
				return "{\"status\":\"" + status + "\", \"response\":\"OTP expired\"}";
				// return false;
			}
			// Expire OTP based on configuration
			// expireOtpOnHit = properties.getProperty("expireOtpOnHit");
			expireOtpOnHit = "Y";
			if (expireOtpOnHit.contentEquals("Y")) {
				otpbean.setIsExpired("Y");
				otpbean = otpdao.saveOtp(otpbean);
			}
			// return result
			// return true;
			status = "success";
			return "{\"status\":\"" + status + "\", \"response\":\"OTP verified\"}";
		} catch (Exception e) {
			log.info("Unknown Exception");
			e.printStackTrace();
			// return false;
			status = "failure";
			return "{\"status\":\"" + status + "\", \"response\":\"OTP expired\"}";
		}
	}

	@RequestMapping(value = "/validateLogincred/{userName}/{password}", method = RequestMethod.GET, headers = "Accept=application/json")
	public String validateLogincred(@PathVariable String userName, @PathVariable String password)
			throws InvalidKeyException, NoSuchAlgorithmException, InvalidKeySpecException,
			InvalidAlgorithmParameterException, UnsupportedEncodingException, IllegalBlockSizeException,
			BadPaddingException {
		payplatter.bean.PayerBean payerBean = new payplatter.bean.PayerBean();

		String statusmsg = null;
		String statusCode = null;
		LoginDAO loginDAO = new LoginDAO();
		try {
			payerBean = loginDAO.validateLoginCredentialsDao(userName, password);
log.info("Payer ID ISSS :::::::::: " +payerBean.getId());

log.info("Payer name ISSS :::::::::: " +payerBean.getFirstName());

			String id=payerBean.getId();
			if(payerBean.getStatus().equals("Pass")){
				statusmsg = "Login Successfully Done!!!!!!";
				statusCode = "0000";
				return "{\"status\":\"success\",\"LoginMSG\":\"" + statusmsg + "\",\"StatusCode\":\"" + statusCode
						+ "\",\"UserID\":\"" + payerBean.getId() + "\",\"UserName\":\"" + payerBean.getFirstName()+" "+ payerBean.getLastName() + "\",\"emailId\":\"" + payerBean.getEmailId() + "\"}";

			}
			/* if (!payerBean.getId().equals(0)) {
				statusmsg = "Login Successfully Done!!!!!!";
				statusCode = "0000";
				return "{\"status\":\"success\",\"LoginMSG\":\"" + statusmsg + "\",\"StatusCode\":\"" + statusCode
						+ "\",\"UserID\":\"" + payerBean.getId() + "\",\"UserName\":\"" + payerBean.getFirstName()+" "+ payerBean.getLastName() + "\" }";
			}*/ else{
				statusCode = "0001";
				String status1 = "Login Failed";
				statusmsg = "UserID or Password doesn't match!!!!!!!!!!!!";
				// return loginBean;
				return "{\"status\":\"" + status1 + "\",\"StatusCode\":\"" + statusCode + "\",\"LoginMSG\":\"" + statusmsg
						+ "\", \"User doesn't exist\"}";
			}/*statusmsg = "Login Successfully Done!!!!!!";
			statusCode = "0000";
			return "{\"status\":\"success\",\"LoginMSG\":\"" + statusmsg + "\",\"StatusCode\":\"" + statusCode
					+ "\",\"UserID\":\"" + payerBean.getId() + "\",\"UserName\":\"" + payerBean.getFirstName()+" "+ payerBean.getLastName() + "\" }";
*/
			
		} catch (Exception e) {
			e.printStackTrace();
			statusCode = "0003";
			String status1 = "Login Failed";
			statusmsg = "UserID or Password doesn't match!!!!!!!!!!!!";
			// return loginBean;
			return "{\"status\":\"" + status1 + "\",\"StatusCode\":\"" + statusCode + "\",\"LoginMSG\":\"" + statusmsg
					+ "\", \"User doesn't exist\"}";
		}
	}

	public LoginBean getLoginBean() {
		return loginBean;
	}

	public void setLoginBean(LoginBean loginBean) {
		this.loginBean = loginBean;
	}

	@ModelAttribute
	public void setVaryResponseHeader(HttpServletResponse response) {
		response.setHeader("Access-Control-Allow-Origin", "*");
	}

}
