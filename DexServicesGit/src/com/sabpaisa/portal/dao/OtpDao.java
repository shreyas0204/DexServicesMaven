/**
 * 
 */
package sabpaisa.portal.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import sabpaisa.portal.model.OrganizationsBean;
import sabpaisa.portal.model.OtpBean;

/**
 * @author Aditya Mar 11, 2017
 */
public class OtpDao {
	@Autowired
	private SessionFactory sessionFactory;
	static Logger log = Logger.getLogger(OtpDao.class);

	public OtpBean saveOtp(OtpBean saveData) {
		// Declarations

		// Open session from session factory
		Session session = sessionFactory.openSession();
		try {
			session.beginTransaction();
			session.saveOrUpdate(saveData);
			session.getTransaction().commit();
			return saveData;

		} catch (Exception e) {
			e.printStackTrace();
			return saveData;

		} finally {
			// close session
			session.close();
		}
	}

	public OtpBean getOtp(String otp, String mobile_no) {
		OtpBean otpbean = new OtpBean();
		Session session = sessionFactory.openSession();
		try {
			Criteria cr = session.createCriteria(OtpBean.class);
			cr.add(Restrictions.eq("otp", otp));
			cr.add(Restrictions.eq("mobile_no", mobile_no));
			cr.add(Restrictions.eq("isExpired", "N"));
			if (cr.list().size() == 1) {
				otpbean = (OtpBean) cr.list().get(0);
			}
			return otpbean;
		} catch (Exception e) {
			e.printStackTrace();
			return otpbean;
		} finally {
			session.close();
		}
	}

	public void deleteOtpForNumber(String mobile_no) {
		Session session = sessionFactory.openSession();
		try {
			Criteria cr = session.createCriteria(OtpBean.class);
			cr.add(Restrictions.eq("mobile_no", mobile_no));
			Integer resultSize = cr.list().size();
			if (resultSize > 0) {
				log.info("got " + resultSize + " stale otps for this number");
				List<OtpBean>otps=cr.list();
				for (int i = 0; i < resultSize; i++) {
					OtpBean otpbean = (OtpBean)otps.get(i);
					log.info("deleting otp with Id " + otpbean.getOtp_id());
					session.beginTransaction();
					session.delete(otpbean);
					session.getTransaction().commit();
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

}
