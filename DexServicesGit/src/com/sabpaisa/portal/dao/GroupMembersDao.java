package sabpaisa.portal.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import sabpaisa.portal.model.GroupBean;
import sabpaisa.portal.model.GroupMembersBean;

public class GroupMembersDao {
	@Autowired
	private SessionFactory sessionFactory;
/*
	public Integer saveCreatedGroups(GroupBean groupBean) {
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		Integer id = null;

		try {
			System.out.println("In Set Groups in GroupDAO ....................................");
			GroupBean data = new GroupBean();
			id = (Integer) session.save(groupBean);
			transaction.commit();
			return id;
		} catch (Exception e) {
			e.printStackTrace();
			return id;
		} finally {
			session.close();
		}

	}*/

	@SuppressWarnings("unchecked")
	public List<GroupMembersBean> getMemberList(Integer orgID) {
		
		Session session = sessionFactory.openSession();
		List<GroupMembersBean> bean=null;
		try {
			Criteria criteria=session.createCriteria(GroupMembersBean.class);
			criteria.add(Restrictions.eq("groupBean.groupId", orgID));
			bean=criteria.list();
			return bean;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}finally {
			// close session
			session.close();
		}
	}

}
