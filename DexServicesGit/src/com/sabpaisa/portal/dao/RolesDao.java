package sabpaisa.portal.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;

import sabpaisa.portal.model.RolesBean;

public class RolesDao {
	@Autowired
	private SessionFactory sessionFactory;

	public Integer setRoleDescriptions(RolesBean roleBean) {
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		Integer id=null;
		try {
			System.out.println("In Set Role  Descriptions RoleDao....................................");
			RolesBean data=new RolesBean();
		   id=(int) session.save(roleBean);
		   transaction.commit();
			return id;
		} catch (Exception e) {
			e.printStackTrace();
			return id;
		}
		finally {
			session.close();
		}
	}

}
