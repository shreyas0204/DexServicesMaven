package sabpaisa.portal.dao;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.Transaction;

import sabpaisa.portal.model.OrganizationsBean;
import sabpaisa.portal.utilityclasses.DBConnection;
import sabpaisa.portal.utilityclasses.keygenerator;

import java.sql.*;

public class LinkPortDAO {
	static Logger log = Logger.getLogger(LinkPortDAO.class);
	DBConnection conn = new DBConnection();
	keygenerator id = new keygenerator();
	
	Connection con = null;
	Statement stmt = null;
	
	String ADDITIONAL_FIELD1="NA";
	String ADDITIONAL_FIELD2="NA";
	String ADDITIONAL_FIELD3="NA";
	String ADDITIONAL_FIELD4="NA";
	String ADDITIONAL_FIELD5="NA";
	String ADDITIONAL_FIELD6="NA";
	String ADDITIONAL_FIELD7="NA";
	String ADDITIONAL_FIELD8="NA";
	String ADDITIONAL_FIELD9="NA";
	String ADDITIONAL_FIELD10="NA";
	String CLIENT_ID="NA";
	String COLLECTION_AGENT_CODE="NA";
	String COLLECTION_AGENT_NAME="NA";
	String FINAL_PAYABLE_AMOUNT="NA";
	String INTERNAL_APP_API="NA";
	String LAST_PAYMENT_DATE="NA";
	String PAYER_ADDRESS_AREA="NA";
	String PAYER_STREET_ADD1="NA";
	String PAYER_ADDRESS_COUNTRY="NA";
	String PAYER_FIRST_NAME="NA";
	String PAYER_ADDRESS_CITY="NA";
	String PAYER_LAST_NAME="NA";
	String PAYER_ADDRESS_STATE="NA";
	String PAYER_MOBILE="NA";
	String PAYER_EMAIL="NA";
	String PAYER_UNIQUE_ID="NA";
	String PAYER_PHONE="NA";
	String PAYER_CODE="NA";
	String PAYER_ADDRESS_ZIP="NA";
	
	
	
	public boolean saveLinkPortPayerData(String mcode, String payerFName, String payerLName, String email, String mobile, String amount, String address) throws SQLException {

		try {

			con = conn.getConnection();
			stmt = con.createStatement();

			String key = null;
			PAYER_FIRST_NAME=payerFName;
			PAYER_LAST_NAME=payerLName;
			PAYER_MOBILE=mobile;
			PAYER_EMAIL=email;
			CLIENT_ID=mcode;
			PAYER_STREET_ADD1=address;
			FINAL_PAYABLE_AMOUNT=amount;
			
			try {
				key = id.createTransxId();
			} catch (Exception e) {
				// TODO: handle exception
				key = id.anotherLinkKey();
			}
			
			String sql = "INSERT INTO `payplatter`.`client_payer_upload`"
					+ "(`id`,`ADDITIONAL_FIELD1`,`ADDITIONAL_FIELD10`,`ADDITIONAL_FIELD2`,`ADDITIONAL_FIELD3`,`ADDITIONAL_FIELD4`,`ADDITIONAL_FIELD5`,`ADDITIONAL_FIELD6`,"
					+ "`ADDITIONAL_FIELD7`,`ADDITIONAL_FIELD8`,`ADDITIONAL_FIELD9`,`CLIENT_ID`,`COLLECTION_AGENT_CODE`,`COLLECTION_AGENT_NAME`,`CREATED_DATE`,"
					+ "`FINAL_PAYABLE_AMOUNT`,`INTERNAL_APP_API`,`LAST_PAYMENT_DATE`,`LATE_PAYMENT_CHARGES`,`OTHER_CHARGES`,`PAID_AMOUNT`,`PAYER_ADDRESS_AREA`,`PAYER_ADDRESS_CITY`,"
					+ "`PAYER_ADDRESS_COUNTRY`,`PAYER_ADDRESS_STATE`,`PAYER_ADDRESS_ZIP`,`PAYER_CODE`,`PAYER_EMAIL`,`PAYER_FIRST_NAME`,`PAYER_LAST_NAME`,"
					+ "`PAYER_MOBILE`,`PAYER_PHONE`,`PAYER_STREET_ADD1`,`PAYER_STREET_ADD2`,`PAYER_UNIQUE_ID`,`PAYMENT_ORIGINAL_AMOUNT`,`PAYMENT_ORIGINAL_DUE_DATE`,`PAYMENT_TRANSACTION_DATE`,"
					+ "`PAYMENT_TRANSACTION_ID`,`PREVIOUS_PAYMENT_OUSTANDING`,`PRODUCT_TYPE`,`SECRET_KEY`,`SEND_LINK`,`SERVICE_TYPE`,`STATUS`,`CLIENT_CODE`)"

					+ "VALUES(null,'"+ADDITIONAL_FIELD1+"','"+ADDITIONAL_FIELD10+"','"+ADDITIONAL_FIELD2+"','"+ADDITIONAL_FIELD3+"','"+ADDITIONAL_FIELD4+"',"
					+ "'"+ADDITIONAL_FIELD5+"','"+ADDITIONAL_FIELD6+"','"+ADDITIONAL_FIELD7+"','"+ADDITIONAL_FIELD8+"',"
					+ "'"+ADDITIONAL_FIELD9+"','"+CLIENT_ID+"','"+COLLECTION_AGENT_CODE+"','"+COLLECTION_AGENT_NAME+"',CURRENT_TIMESTAMP(),'"+FINAL_PAYABLE_AMOUNT+"',"
					+ "'"+INTERNAL_APP_API+"','NA','0','0','0','"+PAYER_ADDRESS_AREA+"',"
					+ "'"+PAYER_ADDRESS_CITY+"','"+PAYER_ADDRESS_COUNTRY+"','"+PAYER_ADDRESS_STATE+"','"+PAYER_ADDRESS_ZIP+"','"+PAYER_CODE+"','"+PAYER_EMAIL+"',"
					+ "'"+PAYER_FIRST_NAME+"','"+PAYER_LAST_NAME+"','"+PAYER_MOBILE+"','"+PAYER_PHONE+"','"+PAYER_STREET_ADD1+"',NULL,"
					+ "'"+PAYER_UNIQUE_ID+"','0',NULL,NULL,NULL,"
					+ "'0',NULL,'"+key+"','Y','NA','NEW','NA');";

			stmt.executeUpdate(sql);
			System.out.println("Inserted records into the table...");

		} catch (SQLException se) {
			// Handle errors for JDBC
			return false;
		} catch (Exception e) {
			// Handle errors for Class.forName
			return false;
		} finally {
			// finally block used to close resources
			try {
				if (stmt != null)
					con.close();
			} catch (SQLException se) {
				return false;
			}// do nothing
			try {
				if (con != null)
					con.close();
			} catch (SQLException se) {
				return false;
			}// end finally try
		}
		return true;

	}

}
