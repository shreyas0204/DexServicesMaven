package sabpaisa.portal.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import sabpaisa.portal.model.LoginBean;
import sabpaisa.portal.model.OrganizationProductBean;
import sabpaisa.portal.model.OrganizationsBean;
import sabpaisa.portal.model.UsersBean;

public class UsersDao {
	@Autowired
	private SessionFactory sessionFactory;
	
	public List<UsersBean> getProductList() {
		Session session = sessionFactory.openSession();
		List<UsersBean> productBeanList=null;
		try {
			Criteria criteria=session.createCriteria(UsersBean.class).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
			productBeanList=(List<UsersBean>)criteria.list();
			return productBeanList;
		} catch (Exception ex) {
			ex.printStackTrace();
			System.out.println("");
			return null;
		} finally {
			session.close();
		}
	}

	public List<UsersBean> getRegisteredStudent(Integer partnerId) {
		Session session = sessionFactory.openSession();
		List<UsersBean> productBeanList=null;
		try {
			System.out.println("In side the DAO");
			Criteria criteria=session.createCriteria(UsersBean.class);
			criteria.add(Restrictions.eq("organization_id", partnerId));
			
			
			productBeanList=(List<UsersBean>)criteria.list();
			return productBeanList;
		} catch (Exception ex) {
			ex.printStackTrace();
			System.out.println("");
			return null;
		} finally {
			session.close();
		}
	}

	public Integer saveUserDetails(UsersBean savedata) {
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		
		try {
			System.out.println("In side the saveUserDetails in user master....................................");
			UsersBean data=new UsersBean();
			OrganizationsBean orgBean=new OrganizationsBean();
			int userID=(int) session.save(savedata);
			transaction.commit();
			return userID;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		finally {
			session.close();
		}
	}

	public int saveOrganizationUser(OrganizationsBean orgBean, UsersBean userbean) {
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		try {

			String sql="insert into sabpaisaportal.organizations_users values("+orgBean.getOrganizationId()+","+userbean.getUserId()+")";
			SQLQuery sqlQuery = session.createSQLQuery(sql);
			sqlQuery.executeUpdate();
			transaction.commit();
		} catch (Exception e) {
		 e.printStackTrace();
		}
		finally{
			session.close();
		}
		return 0;
	}

		public List<UsersBean> getUserProfile(String userIds) {
		int userId=Integer.parseInt(userIds);
		Session session = sessionFactory.openSession();
		List<UsersBean> bean=null;
		try {
			Criteria criteria = session.createCriteria(UsersBean.class);
			criteria.add(Restrictions.eq("userId", userId));
			bean=(List<UsersBean>) criteria.list();
			
			return bean;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}finally {
			session.close();
		}
		
	}
	
	
}

