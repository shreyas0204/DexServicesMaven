package sabpaisa.portal.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import sabpaisa.portal.model.GroupBean;
import sabpaisa.portal.model.OrganizationsUsersBean;

import com.sun.xml.internal.messaging.saaj.util.FinalArrayList;

public class GroupDao {
	@Autowired
	private SessionFactory sessionFactory;
	
	public List<GroupBean> getGroupsUsers(Integer orgID) {
		Session session = sessionFactory.openSession();
		List<GroupBean> groupBean=null;
		try {
			System.out.println("In side the DAO Method:::::::::::::::");
			Criteria criteria=session.createCriteria(GroupBean.class);
			criteria.add(Restrictions.eq("orgBean.organizationId", orgID)).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
			groupBean=criteria.list();
			
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}

		return groupBean;
	}

	public List<GroupBean> getAllGroups() {
		Session session = sessionFactory.openSession();
		List<GroupBean> groupBean=null;
		try {
			System.out.println("In side the DAO Method:::::::::::::::");
			Criteria criteria=session.createCriteria(GroupBean.class).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
			groupBean=criteria.list();
			return groupBean;
		} catch (Exception e) {
			e.printStackTrace();
			return groupBean;
		}finally {
			session.close();
		}
		
	}
	

	public Integer saveCreatedGroups(GroupBean groupBean) {
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		Integer id = null;

		try {
			System.out.println("In Set Groups in GroupDAO ....................................");
			GroupBean data = new GroupBean();
			id = (Integer) session.save(groupBean);
			transaction.commit();
			return id;
		} catch (Exception e) {
			e.printStackTrace();
			return id;
		} finally {
			session.close();
		}

	}

	public GroupBean getGroupID(String gropuName) {
		Session session = sessionFactory.openSession();
		GroupBean groupBean=null;
		try {
			groupBean=(GroupBean) session.createCriteria(GroupBean.class).add(Restrictions.eq("groupName", gropuName)).list().iterator()
					.next();
			return groupBean;
		} catch (Exception e) {
			e.printStackTrace();
			return groupBean;
		}
		finally{
			session.close();
		}
		
	}
}
