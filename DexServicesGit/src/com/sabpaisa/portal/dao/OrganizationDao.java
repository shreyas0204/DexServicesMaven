package sabpaisa.portal.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import sabpaisa.portal.model.OrganizationsBean;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

public class OrganizationDao {

	static Logger log = Logger.getLogger(OrganizationDao.class);

	@Autowired
	private SessionFactory sessionFactory;

	@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
	public boolean saveOrganization(OrganizationsBean organizationBean) {
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		try {
			System.out.println("In the organization Dao Class for saving the organization Details::::::::::::::");
			session.save(organizationBean);
			transaction.commit();

		} catch (NullPointerException ex) {
			return false;
		} finally {
			session.close();
		}
		return true;

	}

	public List<OrganizationsBean> getOrganizationList() {
		Session session = sessionFactory.openSession();
		List<OrganizationsBean> organizationBeanList = null;
		try {
			Criteria criteria = session.createCriteria(OrganizationsBean.class).setResultTransformer(
					Criteria.DISTINCT_ROOT_ENTITY);

			organizationBeanList = criteria.list();

			System.out.println("organizationBeanList list size in DAO :::" + organizationBeanList.size());

		} catch (Exception ex) {
			System.out.println("Exception");
			ex.printStackTrace();

		} finally {
			session.close();
		}
		return organizationBeanList;
	}

	public Integer saveOrganizationDetails(OrganizationsBean orgBean) {
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		Integer orgID;
		try {
			System.out.println("In side the saveUserDetails in user master....................................");
			OrganizationsBean data = new OrganizationsBean();
			orgID = (Integer) session.save(orgBean);
			System.out.println("Return Organization Bean::::::::::" + orgID);
			transaction.commit();
			return orgID;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}

	}

	public OrganizationsBean getOrganizationID(String organizationName) {
		Session session = sessionFactory.openSession();
		OrganizationsBean bean = null;
		try {
			bean = (OrganizationsBean) session.createCriteria(OrganizationsBean.class)
					.add(Restrictions.eq("organization_name", organizationName)).list().iterator().next();

		} catch (NullPointerException ec) {

			ec.printStackTrace();
			return bean;
		} finally {
			session.close();
		}

		return bean;
	}

	public Integer saveOrganizations(OrganizationsBean orgBean) {

		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		Integer id = null;
		try {
			System.out.println("In Set Oranization....................................");
			id = (Integer) session.save(orgBean);
			transaction.commit();
			return id;
		} catch (Exception e) {
			return id;
		} finally {
			session.close();
		}

	}


/**
 * @author Aditya
 * Mar 10, 2017
 */
	public List<OrganizationsBean> getOrganizationByID(List<Integer> orgIds) {
		Session session = sessionFactory.openSession();
		List<OrganizationsBean> orgs = new ArrayList<OrganizationsBean>();
		try {
			log.info("Getting organizations with ids: " + orgIds.toString());
			Criteria cr = session.createCriteria(OrganizationsBean.class);
			cr.add(Restrictions.in("organizationId", orgIds));
			orgs = cr.list();
			log.info("Got " + orgs.size() + " organizations from db");
			return orgs;

		} catch (NullPointerException ec) {
			ec.printStackTrace();
			return orgs;
		} finally {
			session.close();
		}
	}

}
