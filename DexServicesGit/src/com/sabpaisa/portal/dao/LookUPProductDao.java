package sabpaisa.portal.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import sabpaisa.portal.model.LookUpBean;

public class LookUPProductDao {
	@Autowired
	private SessionFactory sessionFactory;
	
	public List<LookUpBean> getProductType() {
		Session session=sessionFactory.openSession();
		List<LookUpBean> beanList=null;
		try {
			System.out.println("in side the looKup bena list method ::::::::::::;");
			
			Criteria criteria=session.createCriteria(LookUpBean.class);
			criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
			beanList=criteria.list();
			return beanList;
		} catch (Exception e) {
			e.printStackTrace();
			return beanList;
		}
		finally{
			session.close();
		}
	}

}
