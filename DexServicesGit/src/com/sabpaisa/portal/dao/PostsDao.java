package sabpaisa.portal.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import sabpaisa.portal.model.GroupBean;
import sabpaisa.portal.model.OrganizationProductBean;
import sabpaisa.portal.model.PostsBean;

public class PostsDao {
	@Autowired
	private SessionFactory sessionFactory;

	public List<PostsBean> getOrganizationList(Integer orgID) {
		Session session = sessionFactory.openSession();
		List<PostsBean> postBean = null;
		try {

			Criteria criteria = session.createCriteria(PostsBean.class);
			criteria.add(Restrictions.eq("orgBean.organizationId", orgID));
			// criteria.add(Restrictions.eq("productId", orgID));
			postBean = (List<PostsBean>) criteria.list();
			System.out.println("List size is :::::" + postBean.size());
			return postBean;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}

	}

	public Integer savePostsData(PostsBean postsBean) {
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		Integer id = null;
		try {
			System.out.println("In Set Role  Descriptions RoleDao....................................");
			id = (Integer) session.save(postsBean);
			transaction.commit();
			return id;
		} catch (Exception e) {
			e.printStackTrace();
			return id;
		} finally {
			session.close();
		}

	}

	public List<PostsBean> getPostsList(GroupBean grupBean) {
		Session session = sessionFactory.openSession();
		List<PostsBean> postBean = null;
		try {
			Criteria criteria = session.createCriteria(PostsBean.class);
			criteria.add(Restrictions.eq("grpBean.groupId", grupBean.getGroupId()));
			// criteria.add(Restrictions.eq("productId", orgID));
			postBean = (List<PostsBean>) criteria.list();
			System.out.println("List size is :::::" + postBean.size());
			return postBean;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}

	public List<PostsBean> getAllPostsList() {
		Session session = sessionFactory.openSession();
		List<PostsBean> postBean = null;
		try {
			Criteria criteria = session.createCriteria(PostsBean.class).setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
			
			postBean = criteria.list();
			System.out.println("List size is :::::" + postBean.size());
			return postBean;

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}
}