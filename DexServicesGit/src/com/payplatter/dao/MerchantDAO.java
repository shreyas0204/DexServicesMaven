package payplatter.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;

import payplatter.bean.MerchantBean;
import payplatter.bean.MerchantServicesListBean;
import sabpaisa.portal.utilityclasses.DBConnection;

public class MerchantDAO {
	static Logger log = Logger.getLogger(MerchantDAO.class);
	DBConnection conn = new DBConnection();
	Connection con = null;
	Statement stmt = null;

	public List<MerchantBean> getMerchantList(String pid) throws SQLException {

		
		Connection con = conn.getConnection();
		ArrayList<Integer> merchantIdList = new ArrayList<>();
		List<MerchantBean> merchantList = new ArrayList<>();
		PreparedStatement ps = null;
		try {
			String sql = "select merchantId_Fk from payer_merchant_config where payerId_Fk=?";
			ps = con.prepareStatement(sql);
			ps.setInt(1, Integer.valueOf(pid));
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				merchantIdList.add(rs.getInt("merchantId_Fk"));
			}
			log.info("merchantIdList.Size=" + merchantIdList.size());
			if (merchantIdList.size() > 0) {
				merchantList = getAllMerchant(merchantIdList);
				return merchantList;
			}

		} catch (Exception e) {
			System.out.println(e);
		} finally {
			ps.close();
			con.close();
		}
		return merchantList;
	}

	private List<MerchantBean> getAllMerchant(ArrayList<Integer> merchantIdList) throws SQLException {
		String str = new String();
		List<MerchantBean> merchantList = new ArrayList<>();
		Iterator<Integer> iterator = merchantIdList.iterator();
		while (iterator.hasNext()) {
			String string2 = iterator.next().toString();
			str = str + "," + "'" + string2 + "'";
		}
		str = str.substring(1);
		log.info("str is ==" + str);
		con = conn.getConnection();
		ResultSet rs = null;
		try {
			stmt = con.createStatement();
			String sql = "SELECT * FROM merchants_master where Id in (" + str + ")";
			rs = stmt.executeQuery(sql);
			MerchantBean bean = null;
			while (rs.next()) {
				bean = new MerchantBean();
				bean.setId(rs.getInt("Id"));
				bean.setBusiness_Name(rs.getString("Business_Name"));
				merchantList.add(bean);
			}

		} catch (Exception e) {
			System.out.println(e);
		} finally {
			con.close();
			rs.close();
			stmt.close();
		}

		return merchantList;
	}

	public List<MerchantServicesListBean> getMerchantServicesList(String mid) throws SQLException {
		Connection con = conn.getConnection();
		ArrayList<Integer> servicesIdList = new ArrayList<>();
		List<MerchantServicesListBean> merchantList = new ArrayList<>();
		PreparedStatement ps = null;
		try {
			String sql = "select servicesId_Fk from master_services where mid_Fk=?";
			ps = con.prepareStatement(sql);
			ps.setInt(1, Integer.valueOf(mid));
			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				servicesIdList.add(rs.getInt("servicesId_Fk"));
			}
			log.info("servicesIdList.Size=" + servicesIdList.size());
			if (servicesIdList.size() > 0) {
				merchantList = getAllMerchantServices(servicesIdList);
				return merchantList;
			}

		} catch (Exception e) {
			System.out.println(e);
		} finally {
			ps.close();
			con.close();
		}
		return merchantList;
	}

	private List<MerchantServicesListBean> getAllMerchantServices(ArrayList<Integer> servicesIdList)
			throws SQLException {
		String str = new String();
		List<MerchantServicesListBean> merchantServicesList = new ArrayList<>();
		Iterator<Integer> iterator = servicesIdList.iterator();
		while (iterator.hasNext()) {
			String string2 = iterator.next().toString();
			str = str + "," + "'" + string2 + "'";
		}
		str = str.substring(1);
		log.info("str is ==" + str);
		con = conn.getConnection();
		ResultSet rs = null;
		try {
			stmt = con.createStatement();
			String sql = "SELECT * FROM master_services_config msc INNER JOIN lookup_products lp ON msc.productId_Fk = lp.Id where msc.Id in ("
					+ str + ")";
			rs = stmt.executeQuery(sql);
			MerchantServicesListBean bean = null;
			while (rs.next()) {
				bean = new MerchantServicesListBean();
				bean.setId(rs.getInt("Id"));
				bean.setServicesName(rs.getString("description"));
				bean.setProductId(rs.getInt("Id"));
				bean.setProduct_name(rs.getString("product_name"));
				bean.setProduct_config_link(rs.getString("product_config_link"));
				bean.setProduct_description(rs.getString("product_description"));
				bean.setProduct_image(rs.getString("product_image"));
				bean.setProduct_link(rs.getString("product_link"));
				bean.setProduct_tag_line(rs.getString("product_tag_line"));
				bean.setSubscription_price(rs.getString("subscription_price"));

				merchantServicesList.add(bean);
			}

		} catch (Exception e) {
			System.out.println(e);
		} finally {
			con.close();
			rs.close();
			stmt.close();
		}

		return merchantServicesList;
	}

	public MerchantBean getMerchantBean(String mid) throws SQLException {

		MerchantBean merchantBean = new MerchantBean();
		con = conn.getConnection();
		ResultSet rs = null;
		try {

			stmt = con.createStatement();
			String sql = "SELECT * FROM merchants_master where Id='" + mid + "'";
			rs = stmt.executeQuery(sql);
			while (rs.next()) {
				merchantBean.setId(rs.getInt("Id"));
				merchantBean.setFName(rs.getString("FName"));
				merchantBean.setLName(rs.getString("LName"));
				merchantBean.setEmailId(rs.getString("EmailId"));
				merchantBean.setLegalBusinessName(rs.getString("LegalBusinessName"));
				merchantBean.setContact(rs.getString("Contact"));
			}

		} catch (Exception e) {
			System.out.println(e);
		} finally {
			con.close();
			rs.close();
			stmt.close();
		}
		return merchantBean;
	}

}
