package payplatter.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;

import payplatter.bean.LoginBean;
import payplatter.bean.PayerBean;
import sabpaisa.portal.utilityclasses.DBConnection;

public class LoginDAO {
	static Logger log = Logger.getLogger(LoginDAO.class);
	DBConnection conn = new DBConnection();
	Connection con = null;
	Statement stmt = null;

	public PayerBean validateLoginCredentialsDao(String userName, String password) throws SQLException {
		// TODO Auto-generated method stub
		boolean status = false;
		con = conn.getConnection();
		int id = 0;
		LoginBean lBean = new LoginBean();
		PayerBean pbean = new PayerBean();
		PayerDAO payerDao = new PayerDAO();
		PreparedStatement ps = null;
		try {
			String sql = "select * from login_users where userName=? and password=?";
			ps = con.prepareStatement(sql);
			ps.setString(1, userName);
			ps.setString(2, password);
			ResultSet rs = ps.executeQuery();
			status = rs.next();
			if (status) {

				lBean.setId(rs.getInt("payerId_Fk"));
				pbean = payerDao.getPayerProfile(lBean.getId().toString());
				pbean.setStatus("Pass");
			} else {
				pbean.setId("0");
				pbean.setStatus("Fail");

			}
			return pbean;
		} catch (Exception e) {
			System.out.println(e);
		} finally {
			ps.close();
			con.close();
		}
		return pbean;
	}

}
