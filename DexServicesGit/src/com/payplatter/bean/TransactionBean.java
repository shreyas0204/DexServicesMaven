package payplatter.bean;

import java.util.Date;

public class TransactionBean {
	private Integer id;
	private String spTransId;
	private String transId;
	private String transStatus, transPaymode, transIdExt;
	private Date txnCreateDate;
	private Double transAmount, actAmount;
	private String name, email, contact;
	private String payer_type;
	private Integer merchant_Id_fk;
	private String date;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSpTransId() {
		return spTransId;
	}

	public void setSpTransId(String spTransId) {
		this.spTransId = spTransId;
	}

	public String getTransId() {
		return transId;
	}

	public void setTransId(String transId) {
		this.transId = transId;
	}

	public String getTransStatus() {
		return transStatus;
	}

	public void setTransStatus(String transStatus) {
		this.transStatus = transStatus;
	}

	public String getTransPaymode() {
		return transPaymode;
	}

	public void setTransPaymode(String transPaymode) {
		this.transPaymode = transPaymode;
	}

	public String getTransIdExt() {
		return transIdExt;
	}

	public void setTransIdExt(String transIdExt) {
		this.transIdExt = transIdExt;
	}

	public Date getTxnCreateDate() {
		return txnCreateDate;
	}

	public void setTxnCreateDate(Date txnCreateDate) {
		this.txnCreateDate = txnCreateDate;
	}

	public Double getTransAmount() {
		return transAmount;
	}

	public void setTransAmount(Double transAmount) {
		this.transAmount = transAmount;
	}

	public Double getActAmount() {
		return actAmount;
	}

	public void setActAmount(Double actAmount) {
		this.actAmount = actAmount;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public String getPayer_type() {
		return payer_type;
	}

	public void setPayer_type(String payer_type) {
		this.payer_type = payer_type;
	}

	public Integer getMerchant_Id_fk() {
		return merchant_Id_fk;
	}

	public void setMerchant_Id_fk(Integer merchant_Id_fk) {
		this.merchant_Id_fk = merchant_Id_fk;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}
	

}
