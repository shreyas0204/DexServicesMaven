package payplatter.bean;

public class MerchantBean {
	private Integer Id;
	private String FName;
	private String LName;
	private String Business_Name;
	private String LegalBusinessName;
	private String EmailId;
	private String Contact;
	private String Add1;
	private String Add2;
	private String City;
	private String State;
	private String Country;
	private String Zip, have_website, activation_link_key, aadhar, PAN;
	private Integer ind_id, seg_id;

	public Integer getId() {
		return Id;
	}

	public void setId(Integer id) {
		Id = id;
	}

	public String getFName() {
		return FName;
	}

	public void setFName(String fName) {
		FName = fName;
	}

	public String getLName() {
		return LName;
	}

	public void setLName(String lName) {
		LName = lName;
	}

	public String getBusiness_Name() {
		return Business_Name;
	}

	public void setBusiness_Name(String business_Name) {
		Business_Name = business_Name;
	}

	public String getLegalBusinessName() {
		return LegalBusinessName;
	}

	public void setLegalBusinessName(String legalBusinessName) {
		LegalBusinessName = legalBusinessName;
	}

	public String getEmailId() {
		return EmailId;
	}

	public void setEmailId(String emailId) {
		EmailId = emailId;
	}

	public String getContact() {
		return Contact;
	}

	public void setContact(String contact) {
		Contact = contact;
	}

	public String getAdd1() {
		return Add1;
	}

	public void setAdd1(String add1) {
		Add1 = add1;
	}

	public String getAdd2() {
		return Add2;
	}

	public void setAdd2(String add2) {
		Add2 = add2;
	}

	public String getCity() {
		return City;
	}

	public void setCity(String city) {
		City = city;
	}

	public String getState() {
		return State;
	}

	public void setState(String state) {
		State = state;
	}

	public String getCountry() {
		return Country;
	}

	public void setCountry(String country) {
		Country = country;
	}

	public String getZip() {
		return Zip;
	}

	public void setZip(String zip) {
		Zip = zip;
	}

	public String getHave_website() {
		return have_website;
	}

	public void setHave_website(String have_website) {
		this.have_website = have_website;
	}

	public String getActivation_link_key() {
		return activation_link_key;
	}

	public void setActivation_link_key(String activation_link_key) {
		this.activation_link_key = activation_link_key;
	}

	public String getAadhar() {
		return aadhar;
	}

	public void setAadhar(String aadhar) {
		this.aadhar = aadhar;
	}

	public String getPAN() {
		return PAN;
	}

	public void setPAN(String pAN) {
		PAN = pAN;
	}

	public Integer getInd_id() {
		return ind_id;
	}

	public void setInd_id(Integer ind_id) {
		this.ind_id = ind_id;
	}

	public Integer getSeg_id() {
		return seg_id;
	}

	public void setSeg_id(Integer seg_id) {
		this.seg_id = seg_id;
	}

}
